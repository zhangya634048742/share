package com.cube.share.validator.param;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;

/**
 * @author cube.li
 * @since 2022/3/21 22:41
 */
@Getter
@Setter
@ToString
public class PersonSaveParam {

    @NotNull
    private String name;

    private String mobile;

    private String email;
}
