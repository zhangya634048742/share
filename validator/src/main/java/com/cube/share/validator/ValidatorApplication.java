package com.cube.share.validator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author cube.li
 * @since 2022/3/21 22:39
 */
@SpringBootApplication(scanBasePackages = "com.cube.share")
public class ValidatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(ValidatorApplication.class, args);
    }
}
