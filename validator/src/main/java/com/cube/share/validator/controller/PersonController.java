package com.cube.share.validator.controller;

import com.cube.share.base.templates.ApiResult;
import com.cube.share.validator.param.PersonSaveParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * @author cube.li
 * @since 2022/3/21 22:42
 */
@RestController
@RequestMapping("/person")
public class PersonController {

    @PostMapping("/save")
    public ApiResult<Long> save(@Valid @RequestBody PersonSaveParam param) {
        System.out.println(param);
        return ApiResult.success(0L);
    }

    @PostMapping("/single")
    public ApiResult<Integer> single(Integer code) {
        return ApiResult.success(code);
    }

    @PostMapping("/nonParam")
    public ApiResult<Void> nonParam() {
        return ApiResult.success();
    }

    @PostMapping("/batch")
    public ApiResult<List<Integer>> batch(@RequestBody List<Integer> codes) {
        return ApiResult.success(codes);
    }
}
