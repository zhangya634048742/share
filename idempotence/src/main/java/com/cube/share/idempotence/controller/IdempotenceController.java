package com.cube.share.idempotence.controller;

import com.cube.share.base.templates.ApiResult;
import com.cube.share.base.utils.IpUtil;
import com.cube.share.idempotence.annotations.SupportIdempotence;
import com.cube.share.idempotence.service.IdempotenceTokenService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author litb
 * @date 2021/3/11 15:37
 * @description
 */
@RestController
public class IdempotenceController {

    @Resource
    private IdempotenceTokenService idempotenceTokenService;

    @GetMapping("/token")
    public ApiResult getToken(HttpServletRequest request) {
        return ApiResult.success(idempotenceTokenService.generateIdempotenceToken(IpUtil.getIpAddress(request)));
    }

    @DeleteMapping("/delete")
    @SupportIdempotence
    public ApiResult delete() {
        return ApiResult.success("删除成功");
    }

    @PostMapping("/submit")
    @SupportIdempotence(alert = true)
    public ApiResult submit() {
        return ApiResult.success("提交成功");
    }

    @PutMapping("/update")
    @SupportIdempotence(ignoreValue = false, alert = true)
    public ApiResult update() {
        return ApiResult.success("更新,不忽略value对其进行校验");
    }
}
