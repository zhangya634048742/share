package com.cube.share.idempotence.annotations;

import com.cube.share.base.constants.Constant;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author litb
 * @date 2021/3/11 14:17
 * @description 支持幂等性注解
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface SupportIdempotence {

    /**
     * 幂等性校验的token前缀
     */
    String prefix() default Constant.IDEMPOTENCE_TOKEN_PREFIX;

    /**
     * 如果是重复提交,是否需要给出警告信息,默认是不需要
     * 如果为true,在抛出{@link com.cube.share.base.templates.CustomException}时将该
     * 属性作为error_msg,经过全局异常处理器处理后返回至前端
     */
    boolean alert() default false;

    /**
     * 是否需要忽略value,如果true,则不会对redis中的value进行校验,
     * 如果为false,则需要判断是否有key的同时也要判断该key对应的value是否
     * 与指定value相同
     */
    boolean ignoreValue() default true;

}
