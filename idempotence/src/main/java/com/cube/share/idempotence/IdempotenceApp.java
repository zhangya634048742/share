package com.cube.share.idempotence;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author litb
 * @date 2021/3/11 13:42
 * @description 启动类
 */
@SpringBootApplication
@ComponentScan("com.cube.share")
public class IdempotenceApp {

    public static void main(String[] args) {
        SpringApplication.run(IdempotenceApp.class, args);
    }
}
