package com.cube.share.base.constants;

/**
 * @author cube.li
 * @date 2021/6/25 20:47
 * @description
 */
public interface Constant {

    String RESUBMIT_MSG = "请勿重复提交数据";

    String DEFAULT_FILE_CHECK_ERROR_MESSAGE = "文件格式不正确";

    String DEFAULT_FILE_MAX_SIZE_MESSAGE = "文件大小超出限制";

    /**
     * 幂等性校验token前缀
     */
    String IDEMPOTENCE_TOKEN_PREFIX = "IDEMPOTENCE_KEY:";

    /**
     * 幂等性校验token的请求头
     */
    String IDEMPOTENCE_TOKEN_HEADER = "Idempotence-Token";
}
