package com.cube.share.base.templates;

/**
 * @author cube.li
 * @date 2021/11/28 16:28
 * @description Controller基类
 */
public class BaseController {

    public static <T> ApiResult<T> success(T data) {
        return ApiResult.success(data);
    }

    public static ApiResult<Void> success() {
        return ApiResult.success();
    }

}
