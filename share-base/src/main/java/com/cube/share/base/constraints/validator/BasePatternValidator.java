package com.cube.share.base.constraints.validator;

import java.util.regex.Pattern;

/**
 * @author litb
 * @date 2022/3/21 17:56
 */
public class BasePatternValidator extends BaseRequiredValidator {

    protected Pattern pattern;

    public Pattern getPattern() {
        return pattern;
    }

    public void setPattern(Pattern pattern) {
        this.pattern = pattern;
    }
}
