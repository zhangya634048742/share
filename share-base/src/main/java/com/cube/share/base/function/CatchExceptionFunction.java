package com.cube.share.base.function;

import org.apache.commons.lang3.ArrayUtils;

/**
 * @author poker.li
 * @date 2021/7/12 14:51
 * <p>
 * 在执行时捕获异常并打印日志的函数式接口
 */
@FunctionalInterface
@SuppressWarnings("unused")
public interface CatchExceptionFunction {

    /**
     * 执行
     */
    void doInvoke();

    /**
     * 执行方法
     *
     * @param title             执行出错日志标题
     * @param function          待执行的函数式接口
     * @param runAfterException 执行时出现异常后执行的接口
     */
    static void execute(String title, CatchExceptionFunction function, Runnable... runAfterException) {
        function.invoke(title, runAfterException);
    }

    /**
     * 执行方法
     *
     * @param title             打印日志标题
     * @param runAfterException 在出现异常时需要执行的方法
     */
    default void invoke(String title, Runnable... runAfterException) {
        try {
            doInvoke();
        } catch (Exception e) {
            //TODO:日志打印
            System.out.println("title" + e.getMessage());
            if (ArrayUtils.isNotEmpty(runAfterException)) {
                for (Runnable runnable : runAfterException) {
                    runnable.run();
                }
            }
        }
    }
}
