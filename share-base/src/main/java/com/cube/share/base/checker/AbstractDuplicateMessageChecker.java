package com.cube.share.base.checker;

import org.springframework.util.Assert;

import java.io.Serializable;

/**
 * @author poker.li
 * @date 2021/8/17 14:33
 * <p>
 * 抽象重复消息检测器
 */
public abstract class AbstractDuplicateMessageChecker<K extends Serializable> implements DuplicateMessageChecker<K> {

    /**
     * 过期时间,接收到一个不重复的消息后在ttl时间内对该消息做去重
     * 单位 秒
     */
    protected final long ttl;

    public AbstractDuplicateMessageChecker(long ttl) {
        Assert.state(ttl > 0, "ttl must be greater than 0 ");
        this.ttl = ttl;
    }
}
