package com.cube.share.base.checker;

import java.io.Serializable;

/**
 * @author poker.li
 * @date 2021/8/17 14:25
 * <p>
 * 重复消息检测器接口
 * <p>
 * K 消息标识(id)
 */
public interface DuplicateMessageChecker<K extends Serializable> {

    /**
     * 判断是否是重复消息
     *
     * @param messageId 消息id,可以是第三方的消息id,或者又特定字段拼接组成的id
     * @return 是 否
     */
    boolean isDuplicate(K messageId);
}
