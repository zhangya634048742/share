package com.cube.share.base.constraints.validator;

import com.cube.share.base.constraints.ConstraintConstants;
import com.cube.share.base.constraints.Mobile;

/**
 * @author litb
 * @date 2022/3/21 17:50
 * <p>
 * 为{@link Mobile}实现的Validator
 */
public class MobileValidator extends AbstractPatternValidator<Mobile> {

    @Override
    public void initialize(Mobile annotation) {
        required = annotation.required();
        Mobile.Region region = annotation.region();
        switch (region) {
            case MAINLAND:
                pattern = ConstraintConstants.MOBILE_MAINLAND_PATTERN;
                break;
            case HK:
                pattern = ConstraintConstants.MOBILE_HK_PATTERN;
                break;
            case MO:
                pattern = ConstraintConstants.MOBILE_MO_PATTERN;
                break;
            case TW:
                pattern = ConstraintConstants.MOBILE_TW_PATTERN;
                break;
            default:
                throw new IllegalArgumentException("unmatched region");
        }
    }


}
