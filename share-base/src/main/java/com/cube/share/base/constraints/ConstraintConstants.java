package com.cube.share.base.constraints;

import java.util.regex.Pattern;

/**
 * @author litb
 * @date 2022/3/17 9:34
 * <p>
 * Constraints 常量
 */
public interface ConstraintConstants {

    /**
     * 一般的url正则
     */
    String COMMON_URL_REGEX = "(https|http)://[\\w-+&@#/%?=~_|!:,.;]*[\\w-+&@#/%=~_|]";

    /**
     * 小程序url正则
     */
    String MINI_PROGRAM_URL_REGEX = "/?pages/[\\w-+&@#/%?=~_|!:,.;]*[\\w-+&@#/%=~_|]";

    Pattern COMMON_URL_PATTERN = Pattern.compile(COMMON_URL_REGEX);

    Pattern MINI_PROGRAM_URL_PATTERN = Pattern.compile(MINI_PROGRAM_URL_REGEX);

    String IPV4_REGEX = "^(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)\\.(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)\\.(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)\\.(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)$";

    /**
     * 18位身份证号码正则
     */
    String CITIZEN_ID_18_REGEX = "^[1-9]\\d{5}[1-2]\\d{3}((0\\d)|(1[0-2]))(([012]\\d)|3[0-1])\\d{3}(\\d|X|x)$";

    /**
     * 15位身份证号码正则
     */
    String CITIZEN_ID_15_REGEX = "^[1-9]\\d{5}\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{2}$";

    String CITIZEN_ID_REGEX = "(^[1-9]\\d{5}[1-2]\\d{3}((0\\d)|(1[0-2]))(([012]\\d)|3[0-1])\\d{3}(\\d|X|x)$)|(^[1-9]\\d{5}\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{2}$)";

    Pattern CITIZEN_ID_18_PATTERN = Pattern.compile(CITIZEN_ID_18_REGEX);

    Pattern CITIZEN_ID_15_PATTERN = Pattern.compile(CITIZEN_ID_15_REGEX);

    Pattern CITIZEN_ID_PATTERN = Pattern.compile(CITIZEN_ID_REGEX);

    Pattern IPV4_PATTERN = Pattern.compile(IPV4_REGEX);

    String IPV6_REGEX = "(([0-9a-fA-F]{1,4}:){7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]+|::(ffff(:0{1,4})?:)?((25[0-5]|(2[0-4]|1?[0-9])?[0-9])\\.){3}(25[0-5]|(2[0-4]|1?[0-9])?[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1?[0-9])?[0-9])\\.){3}(25[0-5]|(2[0-4]|1?[0-9])?[0-9]))";

    Pattern IPV6_PATTERN = Pattern.compile(IPV4_REGEX);

    String MOBILE_MAINLAND_REGEX = "(?:0|86|\\+86)?1[3-9]\\d{9}";
    /**
     * 中国香港移动电话
     * eg: 中国香港： +852 5100 4810， 三位区域码+10位数字, 中国香港手机号码8位数
     * eg: 中国大陆： +86  180 4953 1399，2位区域码标示+13位数字
     * 中国大陆 +86 Mainland China
     * 中国香港 +852 Hong Kong
     * 中国澳门 +853 Macao
     * 中国台湾 +886 Taiwan
     */
    String MOBILE_HK_REGEX = "(?:0|852|\\+852)?\\d{8}";
    /**
     * 中国台湾移动电话
     * eg: 中国台湾： +886 09 60 000000， 三位区域码+号码以数字09开头 + 8位数字, 中国台湾手机号码10位数
     * 中国台湾 +886 Taiwan 国际域名缩写：TW
     */
    String MOBILE_TW_REGEX = "(?:0|886|\\+886)?(?:|-)09\\d{8}";
    /**
     * 中国澳门移动电话
     * eg: 中国台湾： +853 68 00000， 三位区域码 +号码以数字6开头 + 7位数字, 中国台湾手机号码8位数
     * 中国澳门 +853 Macao 国际域名缩写：MO
     */
    String MOBILE_MO_REGEX = "(?:0|853|\\+853)?(?:|-)6\\d{7}";

    Pattern MOBILE_MAINLAND_PATTERN = Pattern.compile(MOBILE_MAINLAND_REGEX);
    /**
     * 中国香港移动电话
     * eg: 中国香港： +852 5100 4810， 三位区域码+10位数字, 中国香港手机号码8位数
     * eg: 中国大陆： +86  180 4953 1399，2位区域码标示+13位数字
     * 中国大陆 +86 Mainland China
     * 中国香港 +852 Hong Kong
     * 中国澳门 +853 Macao
     * 中国台湾 +886 Taiwan
     */
    Pattern MOBILE_HK_PATTERN = Pattern.compile(MOBILE_HK_REGEX);
    /**
     * 中国台湾移动电话
     * eg: 中国台湾： +886 09 60 000000， 三位区域码+号码以数字09开头 + 8位数字, 中国台湾手机号码10位数
     * 中国台湾 +886 Taiwan 国际域名缩写：TW
     */
    Pattern MOBILE_TW_PATTERN = Pattern.compile(MOBILE_TW_REGEX);
    /**
     * 中国澳门移动电话
     * eg: 中国台湾： +853 68 00000， 三位区域码 +号码以数字6开头 + 7位数字, 中国台湾手机号码8位数
     * 中国澳门 +853 Macao 国际域名缩写：MO
     */
    Pattern MOBILE_MO_PATTERN = Pattern.compile(MOBILE_MO_REGEX);

}
