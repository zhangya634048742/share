package com.cube.share.base.templates;

import org.springframework.lang.NonNull;

import java.io.Serializable;

/**
 * @author poker.li
 * @date 2021/7/30 11:05
 * <p>
 * 枚举类父级接口
 */
public interface IEnum<C extends Serializable, V extends Serializable> {

    /**
     * 获取枚举的code
     *
     * @return code
     */
    C getCode();

    /**
     * 获取枚举的value
     *
     * @return value
     */
    V getValue();

    /**
     * 根据code从指定枚举类中获取枚举值
     *
     * @param clazz 枚举类
     * @param code  code
     * @param <E>   枚举的类型
     * @return 枚举值
     */
    @NonNull
    static <E extends Enum<?> & IEnum<? extends Serializable, ? extends Serializable>, C> E formCode(@NonNull Class<E> clazz, @NonNull C code) {
        E[] elements = clazz.getEnumConstants();
        for (E element : elements) {
            if (element.getCode().equals(code)) {
                return element;
            }
        }
        throw new IllegalArgumentException("不合法的枚举code");
    }

}
