package com.cube.share.base.function;

import java.io.Serializable;
import java.util.function.Function;

/**
 * @author poker.li
 * @date 2021/8/8 14:34
 * <p>
 * 支持序列化的Function
 */
@FunctionalInterface
public interface SerializableFunction<T, R> extends Function<T, R>, Serializable {
}
