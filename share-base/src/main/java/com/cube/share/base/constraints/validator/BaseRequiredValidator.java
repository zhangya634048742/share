package com.cube.share.base.constraints.validator;

/**
 * @author litb
 * @date 2022/3/21 17:55
 */
public class BaseRequiredValidator {

    protected boolean required;

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }
}
