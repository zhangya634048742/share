package com.cube.share.base.constraints;

import com.cube.share.base.constraints.validator.MobileValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author litb
 * @date 2022/3/16 10:06
 * <p>
 * 校验手机号是否合法
 */
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Retention(RUNTIME)
@Repeatable(Mobile.List.class)
@Documented
@Constraint(validatedBy = {MobileValidator.class})
public @interface Mobile {

    String message() default "手机号码不合法";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * 是否必须有值
     */
    boolean required() default true;

    /**
     * 号码所在区域
     */
    Region region() default Region.MAINLAND;

    @Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
    @Retention(RUNTIME)
    @Documented
    @interface List {
        Mobile[] value();
    }

    /**
     * 区域
     */
    enum Region {
        /**
         * 大陆 (?:0|86|\+86)?1[3-9]\d{9}
         */
        MAINLAND,
        /**
         * 台湾 (?:0|886|\+886)?(?:|-)09\d{8}
         */
        TW,
        /**
         * 香港 (?:0|852|\+852)?\d{8}
         */
        HK,
        /**
         * 澳门 (?:0|853|\+853)?(?:|-)6\d{7}
         */
        MO
    }
}
