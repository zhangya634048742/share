package com.cube.share.base.templates;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * @author litb
 * @date 2021/3/10 15:09
 * @description 接口响应结果
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class ApiResult<T> {

    private Integer code;

    private String msg;

    private T data;

    public ApiResult(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public static <T> ApiResult<T> success(T data) {
        return new ApiResult<>(200, null, data);
    }

    public static ApiResult<Void> success() {
        return success(null);
    }

    public static <T> ApiResult<T> error(String msg) {
        return new ApiResult<>(100, msg, null);
    }

    public static ApiResult<Void> error() {
        return new ApiResult<>(100, null, null);
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
