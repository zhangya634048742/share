package com.cube.share.placeholder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author poker.li
 * @date 2021/10/19 1:37
 * <p>
 * 启动类
 */
@SpringBootApplication
public class PlaceHolderApplication {

    public static void main(String[] args) {
        SpringApplication.run(PlaceHolderApplication.class, args);
    }
}
