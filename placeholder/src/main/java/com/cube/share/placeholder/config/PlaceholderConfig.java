package com.cube.share.placeholder.config;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * @author poker.li
 * @date 2021/10/19 1:38
 */
@Configuration
@Data
@Slf4j
public class PlaceholderConfig {


    @Value("${custom.config.numberWithDefaultValue:18}")
    private Integer numberWithDefaultValue;

    @Value("${custom.config.count}")
    private Integer count;

    @Value("${custom.config.list}")
    private List<String> list;

    @PostConstruct
    public void init() {
        log.info("loading PlaceholderConfig : {}", this.toString());
    }
}
