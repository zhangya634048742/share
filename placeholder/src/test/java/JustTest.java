import cn.hutool.system.HostInfo;
import com.alibaba.fastjson.JSONObject;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;


/**
 * @author poker.li
 * @date 2021/10/24 13:59
 */
public class JustTest {

    @Test
    public void test() {
        System.out.println(1 << 30);
    }

    @Test
    public void encode() throws UnsupportedEncodingException {
        String str = "fP2kA7C0U3iLMqT%2BkaEP7vZu0%2FXU4tO6dRRB%2BdfDOdPJ7FOcE8uiYzWEzAzhC2HkO9BIyq2PrrwbFk98FXJIpCphDvtYXtLHDQIPcYpR9RjoUemoW1nqueUOM%2FBmSG4lWXlmO4UzQKTF0xUVEgczjoDUN0l7zNMXdQJ9wPkCTKFeVIVhz4q8kpppEkyQ1EH7BydAz9WN0HHoq8QSVfL10mwFcxuI8BCyIOY1uss5Y%2Bd9UZQwBLs1LIj20RFuXHKYaOuIDBExtBOCNeWIOXKX6AdHwg7RAvJFzcgCD0TAXtPdsLwmS1AmcAei%2F8kFMyeBOUL%2FdZVIN9YLdDZTO6GNYQ%3D%3D";
        System.out.println(URLEncoder.encode(str, "UTF-8"));
    }

    private final String url = "http://127.0.0.1:8050?sign={1}&nonce={2}";

    /**
     * 实际上应该是由工厂创建
     */
    private final RestTemplate restTemplate = new RestTemplate();

    @Test
    public void getForEntity() {
        String sign = "this is sign";
        String nonce = "this is nonce";
        ResponseEntity<String> responseEntity = restTemplate.getForEntity(url, String.class, sign, nonce);
        String data = responseEntity.getBody();
    }

    @Test
    public void getForObject() {
        String sign = "this is sign";
        String nonce = "this is nonce";
        //直接获取响应结果
        String data = restTemplate.getForObject(url, String.class, sign, nonce);
    }

    @Test
    public void postForEntity() {
        String sign = "this is sign";
        String nonce = "this is nonce";
        JSONObject request = new JSONObject();
        request.put("contractId", "1112358");
        request.put("staff", "staff");
        ResponseEntity<JSONObject> responseEntity = restTemplate.postForEntity(url, request, JSONObject.class, sign, nonce);
        JSONObject data = responseEntity.getBody();
        //解析data,判断是否执行成功并获取结果
    }

    @Test
    public void postForObject() {
        String sign = "this is sign";
        String nonce = "this is nonce";
        JSONObject request = new JSONObject();
        request.put("contractId", "1112358");
        request.put("staff", "staff");
        JSONObject data = restTemplate.postForObject(url, request, JSONObject.class, sign, nonce);
        //解析data,判断是否执行成功并获取结果
    }

    @Test
    public void exchange() {
        String sign = "this is sign";
        String nonce = "this is nonce";
        JSONObject request = new JSONObject();
        request.put("contractId", "1112358");
        request.put("staff", "staff");

        //添加请求头
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Token", "this is token");
        //构造请求实体
        HttpEntity<JSONObject> requestEntity = new HttpEntity<>(request, headers);

        ResponseEntity<JSONObject> responseEntity =
                restTemplate.exchange(url, HttpMethod.POST, requestEntity, JSONObject.class, sign, nonce);

        //获取响应结果并处理
        JSONObject data = responseEntity.getBody();
    }

    @Test
    public void hostInfo() {
        HostInfo hostInfo = new HostInfo();
        System.out.println(hostInfo);
    }
}
