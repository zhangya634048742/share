import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import java.util.*;

/**
 * @author cube.li
 * @date 2021/4/4 18:54
 * @description
 */
public class GenericTest {

    @Test
    public void generic() {
        List<? extends Number> numbers = new ArrayList<>();

        List<? super Number> nums = new LinkedList<>();
        nums.add(1);
    }

    @org.junit.Test
    public void test() {
        JSONObject jsonObject = new JSONObject();
        Object obj = jsonObject;
        boolean flag = obj instanceof Map;
        System.out.println(flag);
    }
}
