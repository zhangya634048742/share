package com.cube.share.log;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @author cube.li
 * @date 2021/4/3 21:38
 * @description 启动类
 */
@SpringBootApplication
@EnableAspectJAutoProxy
public class LogApplication {

    public static void main(String[] args) {
        SpringApplication.run(LogApplication.class, args);
    }
}
