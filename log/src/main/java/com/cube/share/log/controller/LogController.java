package com.cube.share.log.controller;

import com.cube.share.base.templates.ApiResult;
import com.cube.share.log.annotation.ApiLog;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author cube.li
 * @date 2021/4/4 10:49
 * @description
 */
@RestController
@RequestMapping("/log")
@Slf4j
public class LogController {

    @GetMapping("/info")
    @ApiLog(level = ApiLog.LogLevel.INFO)
    public ApiResult info(@RequestParam("name") String name, @RequestParam("id") Integer id) {
        return ApiResult.success();
    }

    @PostMapping("/upload")
    @ApiLog(excludes = Integer.class)
    public ApiResult upload(MultipartFile file, @RequestParam("fileName") String fileName, @RequestParam("id") Integer id) {
        return ApiResult.success();
    }

    @ApiLog(level = ApiLog.LogLevel.ERROR, title = "标题")
    @RequestMapping("/error")
    public void error() {
    }
}
