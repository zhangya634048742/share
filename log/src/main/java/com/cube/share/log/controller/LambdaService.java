package com.cube.share.log.controller;

/**
 * @author litb
 * @date 2022/3/1 11:17
 */
public class LambdaService {

    public static void printString(String str, Printer<String> printer) {
        printer.print(str);
    }

    public static void main(String[] args) {
        printString("log log log", System.out::println);
    }
}

interface Printer<T> {
    /**
     * 打印
     *
     * @param t t
     */
    void print(T t);
}
