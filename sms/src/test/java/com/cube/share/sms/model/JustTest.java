package com.cube.share.sms.model;

import com.cube.share.base.utils.JacksonUtils;
import com.cube.share.sms.model.param.SmsPlaceHolderParameter;
import org.junit.Test;

import java.util.Map;

/**
 * @author cube.li
 * @date 2021/9/4 12:00
 * @description
 */
public class JustTest {

    @Test
    public void pojo2Map() {
        SmsPlaceHolderParameter parameter = new SmsPlaceHolderParameter();
        parameter.setAddress("中国");
        parameter.setTitle("会展");
        Map<String, Object> map = JacksonUtils.toMap(parameter);
        System.out.println(map);
    }
}
