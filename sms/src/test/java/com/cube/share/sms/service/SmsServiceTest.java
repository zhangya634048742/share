package com.cube.share.sms.service;

import com.cube.share.sms.model.param.SmsPlaceHolderParameter;
import com.cube.share.sms.model.param.SmsSendParam;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * @author cube.li
 * @date 2021/9/4 12:00
 * @description
 */
@SpringBootTest
class SmsServiceTest {

    @Resource
    SmsService smsService;

    @Test
    void send() {
        SmsSendParam smsSendParam = new SmsSendParam();
        smsSendParam.setTemplateCode(1);
        SmsPlaceHolderParameter placeHolderParameter = new SmsPlaceHolderParameter();
        placeHolderParameter.setAddress("上海");
        placeHolderParameter.setCompanyName("有天科技");
        placeHolderParameter.setTitle("人工智能2021");
        placeHolderParameter.setStartTime("2021-9-10");
        //placeHolderParameter.setEndTime("2021-9-20");
        smsSendParam.setParameter(placeHolderParameter);
        smsService.send(smsSendParam);
    }

}