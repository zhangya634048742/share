package com.cube.share.sms.strategy;

import com.cube.share.base.templates.CustomException;
import com.cube.share.sms.model.param.SmsPlaceHolderParameter;
import lombok.extern.slf4j.Slf4j;

import java.util.Set;

/**
 * @author cube.li
 * @date 2021/9/4 11:57
 * @description 短信发送拒绝策略-完全匹配,只有当短信入参与短信模板占位符完全匹配时才允许发送
 */
@Slf4j
public class SmsSendTotallyMatchStrategy implements SmsSendRejectStrategy {

    @Override
    public void reject(SmsTemplateContext templateContext, SmsPlaceHolderParameter parameter) {
        Set<String> parameterKeySet = getParameterSet(parameter);
        if (!parameterKeySet.containsAll(templateContext.getPlaceHolderKeySet())) {
            log.error("短信占位符替换参数与短信模板不完全匹配,templateContent = {},parameter = {}", templateContext.getTemplateContent(), parameter);
            throw new CustomException("短信占位符替换参数与短信模板不完全匹配");
        }
    }
}
