package com.cube.share.sms.strategy;

import com.cube.share.base.utils.PlaceHolderUtils;
import com.cube.share.sms.constant.SmsConstant;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Set;

/**
 * @author cube.li
 * @date 2021/9/4 11:04
 * @description 短信模板上下文信息
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class SmsTemplateContext {

    /**
     * 模板主键id
     */
    private Long templateId;

    /**
     * 模板code
     */
    private Integer templateCode;

    /**
     * 模板分类
     */
    private Integer templateClassify;

    /**
     * 模板内容
     */
    private String templateContent;

    /**
     * 占位符: 前缀 + key + 后缀,例如 {url}
     * 占位符key集合
     */
    private Set<String> placeHolderKeySet;

    public static SmsTemplateContext from(String templateContent, Integer templateCode) {
        SmsTemplateContext context = new SmsTemplateContext();
        context.setTemplateContent(templateContent);
        context.setTemplateCode(templateCode);
        context.setPlaceHolderKeySet(PlaceHolderUtils.getPlaceHolderKeySet(templateContent, SmsConstant.DEFAULT_PLACE_HOLDER_REGEX, SmsConstant.DEFAULT_PLACE_HOLDER_KEY_REGEX));
        return context;
    }

    public int getPlaceHolderKeyCount() {
        return this.placeHolderKeySet.size();
    }

}
