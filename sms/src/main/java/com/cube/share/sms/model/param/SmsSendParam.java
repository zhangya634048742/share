package com.cube.share.sms.model.param;

import lombok.Data;

/**
 * @author cube.li
 * @date 2021/9/4 9:00
 * @description 短信发送参数
 */
@Data
public class SmsSendParam {

    /**
     * 短信模板code
     */
    private Integer templateCode;

    /**
     * 手机号码
     */
    private String mobile;

    /**
     * 占位符参数
     */
    private SmsPlaceHolderParameter parameter;
}
