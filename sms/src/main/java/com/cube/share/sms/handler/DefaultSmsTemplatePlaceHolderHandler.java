package com.cube.share.sms.handler;

import com.cube.share.base.utils.JacksonUtils;
import com.cube.share.base.utils.PlaceHolderUtils;
import com.cube.share.sms.constant.SmsConstant;
import com.cube.share.sms.model.param.SmsPlaceHolderParameter;
import com.cube.share.sms.strategy.SmsSendRejectStrategy;
import com.cube.share.sms.strategy.SmsTemplateContext;

/**
 * @author cube.li
 * @date 2021/9/4 12:27
 * @description 默认的短信模板占位符处理器
 */
public class DefaultSmsTemplatePlaceHolderHandler implements SmsTemplatePlaceHolderHandler {

    private SmsSendRejectStrategy rejectStrategy;

    public DefaultSmsTemplatePlaceHolderHandler(SmsSendRejectStrategy rejectStrategy) {
        this.rejectStrategy = rejectStrategy;
    }

    @Override
    public String handle(SmsTemplateContext templateContext, SmsPlaceHolderParameter parameter) {
        //发送拒绝策略
        rejectStrategy.reject(templateContext, parameter);
        return PlaceHolderUtils.replacePlaceHolder(templateContext.getTemplateContent(),
                JacksonUtils.toMap(parameter),
                SmsConstant.DEFAULT_PLACE_HOLDER_REGEX,
                SmsConstant.DEFAULT_PLACE_HOLDER_KEY_REGEX);
    }
}
