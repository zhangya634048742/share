package com.cube.share.sms.handler;

import com.cube.share.sms.model.param.SmsPlaceHolderParameter;
import com.cube.share.sms.strategy.SmsTemplateContext;

/**
 * @author cube.li
 * @date 2021/9/4 9:37
 * @description 短信占位符处理器
 */
public interface SmsTemplatePlaceHolderHandler {

    /**
     * 替换短信模板中的占位符
     *
     * @param templateContext 短信模板上下文信息
     * @param parameter       替换占位符的参数
     * @return 待发送的短信内容
     */
    String handle(SmsTemplateContext templateContext, SmsPlaceHolderParameter parameter);
}
