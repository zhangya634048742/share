package com.cube.share.sms.model.param;

import lombok.Data;

/**
 * @author cube.li
 * @date 2021/9/8 22:01
 * @description 短信上下文
 */
@Data
public class SmsContext {

    private String mobile;

    private String content;
}
