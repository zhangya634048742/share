package com.cube.share.sms.config;

import com.cube.share.sms.constant.SmsSendRejectStrategyEnum;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

/**
 * @author cube.li
 * @date 2021/9/4 9:04
 * @description 短信配置信息
 */
@ConfigurationProperties(prefix = "sms")
@Configuration
@Data
public class SmsConfig {

    private Map<Integer, String> templates;

    private SmsSendRejectStrategyEnum defaultRejectStrategyEnum;
}
