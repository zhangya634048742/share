package com.cube.share.sms.strategy;

import com.cube.share.sms.model.param.SmsPlaceHolderParameter;

/**
 * @author cube.li
 * @date 2021/9/4 11:54
 * @description 短信发送拒绝策略-忽略策略,无论短信发送入参与模板是否匹配,都允许发送
 */
public class SmsSendIgnoreStrategy implements SmsSendRejectStrategy {

    @Override
    public void reject(SmsTemplateContext templateContext, SmsPlaceHolderParameter parameter) {
        //do nothing
    }
}
