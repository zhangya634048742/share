package com.cube.share.sms.factory;

import com.cube.share.sms.constant.SmsSendRejectStrategyEnum;
import com.cube.share.sms.strategy.SmsSendAnyMatchStrategy;
import com.cube.share.sms.strategy.SmsSendIgnoreStrategy;
import com.cube.share.sms.strategy.SmsSendRejectStrategy;
import com.cube.share.sms.strategy.SmsSendTotallyMatchStrategy;

/**
 * @author cube.li
 * @date 2021/9/4 12:49
 * @description 拒绝策略工厂
 */
public class SmsSendRejectStrategyFactory {

    private static final SmsSendIgnoreStrategy IGNORE_STRATEGY = new SmsSendIgnoreStrategy();

    private static final SmsSendAnyMatchStrategy ANY_MATCH_STRATEGY = new SmsSendAnyMatchStrategy();

    private static final SmsSendTotallyMatchStrategy TOTALLY_MATCH_STRATEGY = new SmsSendTotallyMatchStrategy();

    public static SmsSendRejectStrategy getStrategy(SmsSendRejectStrategyEnum strategyEnum) {
        switch (strategyEnum) {
            case IGNORE:
                return IGNORE_STRATEGY;
            case ANY_MATCH:
                return ANY_MATCH_STRATEGY;
            case TOTALLY_MATCH:
                return TOTALLY_MATCH_STRATEGY;
            default:
                throw new IllegalArgumentException("Illegal StrategyEnum Param");
        }
    }

}
