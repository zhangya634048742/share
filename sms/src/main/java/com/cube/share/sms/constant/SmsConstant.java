package com.cube.share.sms.constant;

/**
 * @author cube.li
 * @date 2021/9/4 11:29
 * @description 短信相关常量
 */
public interface SmsConstant {

    String DEFAULT_PLACE_HOLDER_REGEX = "\\{[a-z].*?\\}";

    String DEFAULT_PLACE_HOLDER_KEY_REGEX = "[^(\\{)|(\\})]+";
}
