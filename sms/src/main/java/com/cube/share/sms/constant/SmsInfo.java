package com.cube.share.sms.constant;

import com.cube.share.sms.model.param.SmsContext;
import com.cube.share.sms.strategy.SmsTemplateContext;

/**
 * @author cube.li
 * @date 2021/9/8 22:02
 * @description
 */
public class SmsInfo {

    private static final ThreadLocal<SmsContext> SMS_CONTEXT_THREAD_LOCAL = new ThreadLocal<>();

    private static final ThreadLocal<SmsTemplateContext> SMS_TEMPLATE_CONTEXT_THREAD_LOCAL = new ThreadLocal<>();

    public static void setSmsContext(SmsContext smsContext) {
        SMS_CONTEXT_THREAD_LOCAL.set(smsContext);
    }

    public static void setSmsTemplateContext(SmsTemplateContext smsTemplateContext) {
        SMS_TEMPLATE_CONTEXT_THREAD_LOCAL.set(smsTemplateContext);
    }

    public static SmsContext getSmsContext() {
        return SMS_CONTEXT_THREAD_LOCAL.get();
    }

    public static SmsTemplateContext getSmsTemplateContext() {
        return SMS_TEMPLATE_CONTEXT_THREAD_LOCAL.get();
    }

    public static void clear() {
        SMS_TEMPLATE_CONTEXT_THREAD_LOCAL.remove();
        SMS_CONTEXT_THREAD_LOCAL.remove();
    }

    public static void from(SmsContext smsContext, SmsTemplateContext smsTemplateContext) {
        setSmsContext(smsContext);
        setSmsTemplateContext(smsTemplateContext);
    }
}
