package com.cube.share.sms.aspect;

import com.cube.share.sms.constant.SmsInfo;
import com.cube.share.sms.model.param.SmsContext;
import com.cube.share.sms.strategy.SmsTemplateContext;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

/**
 * @author cube.li
 * @date 2021/9/8 22:06
 * @description 短信日志注解切面
 */
@Aspect
@Component
@Slf4j
public class SmsLogAspect {

    @Pointcut("@annotation(com.cube.share.sms.aspect.SmsLog)")
    public void pointCut() {
    }

    @AfterReturning(pointcut = "pointCut()", returning = "result")
    public void around(Object result) throws Throwable {
        SmsContext smsContext = SmsInfo.getSmsContext();
        SmsTemplateContext smsTemplateContext = SmsInfo.getSmsTemplateContext();
        log.info("短信正常发送日志,smsContext={},smsTemplateContext={},msgId={}", smsContext, smsTemplateContext, result);
    }

    @AfterThrowing(pointcut = "pointCut()", throwing = "ex")
    public void error(Throwable ex) {
        SmsContext smsContext = SmsInfo.getSmsContext();
        SmsTemplateContext smsTemplateContext = SmsInfo.getSmsTemplateContext();
        log.error("短信发送异常日志,smsContext={},smsTemplateContext={},msg={}", smsContext, smsTemplateContext, ex.getMessage());
    }
}
