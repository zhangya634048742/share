package com.cube.share.sms.strategy;

import com.cube.share.base.templates.CustomException;
import com.cube.share.sms.model.param.SmsPlaceHolderParameter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Set;

/**
 * @author cube.li
 * @date 2021/9/4 11:45
 * @description SmsSendAnyMatchStrategy, 只要占位符参数匹配了短信模板中的任意一个占位符key,就允许发送
 */
@Slf4j
public class SmsSendAnyMatchStrategy implements SmsSendRejectStrategy {

    @Override
    public void reject(SmsTemplateContext templateContext, SmsPlaceHolderParameter parameter) {
        Set<String> parameterKeySet = getParameterSet(parameter);
        if (CollectionUtils.intersection(templateContext.getPlaceHolderKeySet(), parameterKeySet).size() <= 0) {
            log.error("短信占位符替换参数与短信模板完全不匹配,templateContent = {},parameter = {}", templateContext.getTemplateContent(), parameter);
            throw new CustomException("短信占位符替换参数与短信模板完全不匹配");
        }
    }
}
