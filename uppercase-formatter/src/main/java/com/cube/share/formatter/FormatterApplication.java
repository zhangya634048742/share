package com.cube.share.formatter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author cube.li
 * @date 2021/4/4 21:48
 * @description 启动类
 */
@SpringBootApplication
public class FormatterApplication {

    public static void main(String[] args) {
        SpringApplication.run(FormatterApplication.class, args);
    }
}
