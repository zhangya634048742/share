package com.cube.share.formatter.letter;

import com.cube.share.formatter.annotation.LetterSensitive;
import org.springframework.format.AnnotationFormatterFactory;
import org.springframework.format.Parser;
import org.springframework.format.Printer;

import java.util.Collections;
import java.util.Set;

/**
 * @author cube.li
 * @date 2021/4/4 22:44
 * @description 注解驱动的格式器工厂
 */
public final class LetterFormatterFactory implements AnnotationFormatterFactory<LetterSensitive> {

    @Override
    public Set<Class<?>> getFieldTypes() {
        return Collections.singleton(String.class);
    }

    @Override
    public Printer<?> getPrinter(LetterSensitive annotation, Class<?> fieldType) {
        return letterFormatter(annotation);
    }

    @Override
    public Parser<?> getParser(LetterSensitive annotation, Class<?> fieldType) {
        return letterFormatter(annotation);
    }

    private LetterFormatter letterFormatter(LetterSensitive annotation) {
        return new LetterFormatter(annotation.position(), annotation.type(), annotation.length(), annotation.lenient());
    }
}
