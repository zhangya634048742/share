package com.cube.share.formatter.controller;

import com.cube.share.base.templates.ApiResult;
import com.cube.share.formatter.model.MockUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * @author cube.li
 * @date 2021/4/5 19:05
 * @description
 */
@RestController
@Slf4j
public class FormatterController {

    @PostMapping("user")
    public ApiResult<Void> save(@RequestBody MockUser user) {
        log.info("user = {}", user.toString());
        return ApiResult.success();
    }

    @GetMapping("/user")
    public ApiResult<MockUser> get() {
        return ApiResult.success(new MockUser("li", "cube", "shanghai", new Date()));
    }
}
