package com.cube.share.formatter.annotation;

import java.lang.annotation.*;

/**
 * @author cube.li
 * @date 2021/4/4 22:14
 * @description 字母敏感注解, 对字符串指定位置处的字母进行大小写转换
 */
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
public @interface LetterSensitive {

    /**
     * 从起始位置处需要转换的字符长度,如果{@code Position}指定为All则该属性不生效
     */
    int length() default 1;

    Type type() default Type.TO_UPPER;

    Position position() default Position.LEFT;

    /**
     * 对转换的位置是否是宽容的
     * 如果为false,则当指定的位置超过了给定字符串的位置时抛出异常,
     * 例如,字符串"abcd",如果指定length=5,则抛出异常
     * 如果为true,超过了给定的位置不会抛出异常,但是原字符串可能并未发生转换,
     * 如果{@code Position}指定为All则该属性不生效
     */
    boolean lenient() default false;


    /**
     * 转换类型
     */
    enum Type {
        /**
         * 小写转大写
         */
        TO_UPPER,
        /**
         * 大写转小写
         */
        TO_LOWER
    }

    /**
     * 转换的起始位置
     */
    enum Position {
        /**
         * 从最左边开始
         */
        LEFT,
        /**
         * 从最右边开始
         */
        RIGHT,
        /**
         * 所有位置
         */
        ALL
    }

}
