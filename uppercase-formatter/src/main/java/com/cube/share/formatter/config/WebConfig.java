package com.cube.share.formatter.config;

import com.cube.share.formatter.letter.LetterFormatterFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author cube.li
 * @date 2021/4/5 19:03
 * @description
 */
@Configuration
@Slf4j
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addFormatters(FormatterRegistry registry) {
        log.info("配置 LetterSensitive 注解格式化器...");
        registry.addFormatterForFieldAnnotation(new LetterFormatterFactory());
    }
}
