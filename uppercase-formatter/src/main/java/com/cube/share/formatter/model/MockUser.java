package com.cube.share.formatter.model;

import com.cube.share.formatter.annotation.LetterSensitive;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author cube.li
 * @date 2021/4/5 19:06
 * @description
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class MockUser implements Serializable {

    private static final long serialVersionUID = 2508970858657905887L;

    @LetterSensitive
    private String firstName;

    @LetterSensitive(position = LetterSensitive.Position.RIGHT, length = 2, lenient = true)
    private String lastName;

    @LetterSensitive(position = LetterSensitive.Position.ALL, type = LetterSensitive.Type.TO_LOWER)
    private String address;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date date;

}
