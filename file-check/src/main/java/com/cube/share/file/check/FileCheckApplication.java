package com.cube.share.file.check;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author cube.li
 * @date 2021/6/25 20:02
 * @description
 */
@SpringBootApplication
@ComponentScan("com.cube.share")
public class FileCheckApplication {

    public static void main(String[] args) {
        SpringApplication.run(FileCheckApplication.class, args);
    }
}
