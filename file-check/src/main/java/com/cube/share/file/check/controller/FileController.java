package com.cube.share.file.check.controller;

import com.cube.share.base.templates.ApiResult;
import com.cube.share.file.check.annotations.FileCheck;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * @author cube.li
 * @date 2021/6/25 20:06
 * @description
 */
@RestController
public class FileController {

    @PostMapping("/uploadFile")
    @FileCheck(typeCheckErrorMsg = "不支持的文件格式", maxSize = 1, supportedSuffixes = {"png", "jpg"}, type = FileCheck.CheckType.MAGIC_NUMBER)
    public ApiResult uploadFile(@RequestParam("file") MultipartFile file) throws IOException {
        return ApiResult.success();
    }

    @PostMapping("/uploadFiles")
    @FileCheck(typeCheckErrorMsg = "不支持的文件格式", supportedSuffixes = {"png", "jpg", "jpeg"}, type = FileCheck.CheckType.MAGIC_NUMBER)
    public ApiResult uploadFiles(@RequestParam("files") MultipartFile[] files) {
        return ApiResult.success();
    }
}
