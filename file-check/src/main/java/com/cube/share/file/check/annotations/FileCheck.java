package com.cube.share.file.check.annotations;

import com.cube.share.file.check.enums.FileType;

import java.lang.annotation.*;

import static com.cube.share.base.constants.Constant.DEFAULT_FILE_CHECK_ERROR_MESSAGE;
import static com.cube.share.base.constants.Constant.DEFAULT_FILE_MAX_SIZE_MESSAGE;

/**
 * @author cube.li
 * @date 2021/6/25 20:19
 * @description 文件校验
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface FileCheck {

    /**
     * 格式校验不通过提示信息
     *
     * @return
     */
    String typeCheckErrorMsg() default DEFAULT_FILE_CHECK_ERROR_MESSAGE;

    /*
    校验方式
     */
    CheckType type() default CheckType.SUFFIX;

    /**
     * 支持的文件后缀
     *
     * @return
     */
    String[] supportedSuffixes() default {};

    /**
     * 支持的文件类型
     *
     * @return
     */
    FileType[] supportedFileTypes() default {};

    /**
     * 文件的最大大小
     *
     * @return
     */
    long maxSize() default 10;

    /**
     * 文件大小的单位
     *
     * @return
     */
    SizeUnit sizeUnit() default SizeUnit.MB;

    /**
     * 文件大小
     *
     * @return
     */
    String sizeCheckErrorMsg() default DEFAULT_FILE_MAX_SIZE_MESSAGE;

    enum CheckType {
        /**
         * 仅校验后缀
         */
        SUFFIX,
        /**
         * 校验文件头(魔数)
         */
        MAGIC_NUMBER
    }

    /**
     * 文件大小单位
     */
    enum SizeUnit {
        BYTE,
        KB,
        MB
    }
}
