import org.junit.Test;

import java.io.File;
import java.text.ChoiceFormat;
import java.text.Format;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author cube.li
 * @date 2021/6/25 20:04
 * @description
 */
public class JustTest {

    @Test
    public void inputStreamTest() {
        File file = new File("C:\\Users\\li\\Desktop\\常用.txt");
    }

    @Test
    public void tst() {
        System.out.println(1 << 10);
        System.out.println(1 << 10 << 10);
    }

    @Test
    public void choiceMessageFormat() {
        double[] fileLimits = {0, 1, 2};
        String[] filePart = {"are no files", "is one file", "are {2} files"};
        ChoiceFormat choiceFormat = new ChoiceFormat(fileLimits, filePart);
        Format[] testFormats = {choiceFormat, null, NumberFormat.getInstance()};
        MessageFormat messageFormat = new MessageFormat("there  {0} on {1}");
        messageFormat.setFormats(testFormats);
        Object[] testArgs = {null, "ADisk", null};
        for (int i = 0; i < 4; i++) {
            testArgs[0] = i;
            testArgs[2] = testArgs[0];
            System.out.println(messageFormat.format(testArgs));
        }
    }

    @Test
    public void iterator() {
        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        list.add("c");

        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            String str = iterator.next();
            if ("b".equals(str)) {
                iterator.remove();
            }
        }

        System.out.println(list);
    }

}
