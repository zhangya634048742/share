package com.cube.share.cache.constant;

/**
 * @author cube.li
 * @date 2021/7/18 16:52
 * @description RedisCache常量
 */
public interface RedisCacheConstant {

    String DEFAULT_CACHE_MANAGER_BEAN_NAME = "redisCacheManager";
}
