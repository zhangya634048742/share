package com.cube.share.cache.anonotation;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * @author poker.li
 * @date 2021/7/17 16:08
 * <p>
 * 基于{@link org.springframework.cache.annotation.CacheConfig}提供的缓存配置注解
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@CacheConfig
@Inherited
public @interface ICacheConfig {

    /**
     * 缓存前缀名,通过该属性指定不同模块缓存的存放位置,
     * 在Redis中分块展示,对于指定的缓存键key="11235813",存在Redis的实际键为 "sysUser::11235813"
     */
    @AliasFor(annotation = CacheConfig.class, attribute = "cacheNames")
    String[] cacheNames() default {};

    /**
     * 缓存键生成器
     */
    @AliasFor(annotation = CacheConfig.class, attribute = "keyGenerator")
    String keyGenerator() default "";

    /**
     * 缓存管理器,如果没有指定则采用默认的缓存管理器,如果需要自定义缓存的过期时间。
     * 则必须指定该属性,并且要使该属性唯一,这样能创建一个新的RedisCacheManager(bean的名字就是cacheManager)
     */
    @AliasFor(annotation = CacheConfig.class, attribute = "cacheManager")
    String cacheManager() default "";

    @AliasFor(annotation = CacheConfig.class, attribute = "cacheResolver")
    String cacheResolver() default "";

    /**
     * 是否允许缓存存入null
     */
    boolean allowCachingNullValues() default false;

    /**
     * 缓存的有效期限,如果值小于等于0则表示永久保存
     */
    int expire() default 8;

    /**
     * 缓存过期的时间单位
     */
    TimeUnit timeUnit() default TimeUnit.HOURS;

    /**
     * 设置是否兼容事务,
     * 默认是true,只在事务成功提交后才会进行缓存的put/evict操作
     */
    boolean transactionAware() default true;
}
