package com.cube.share.cache.anonotation;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * @author poker.li
 * @date 2021/7/17 17:08
 * <p>
 * 基于{@link Cacheable}实现的缓存存放注解
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@SuppressWarnings("SpringCacheNamesInspection")
@Cacheable
public @interface ICache {

    /**
     * 缓存的名字(缓存键的前缀),例如,指定为"sysUser",
     * 对于指定的缓存键key="11235813",存在Redis的实际键为 "sysUser::11235813"
     */
    @AliasFor(annotation = Cacheable.class, attribute = "value")
    String[] value() default {};

    /**
     * 缓存的名字(缓存键的前缀)
     */
    @AliasFor(annotation = Cacheable.class, attribute = "cacheNames")
    String[] cacheNames() default {};

    /**
     * 缓存键
     */
    @AliasFor(annotation = Cacheable.class, attribute = "key")
    String key() default "";

    /**
     * 缓存键生成器
     */
    @AliasFor(annotation = Cacheable.class, attribute = "keyGenerator")
    String keyGenerator() default "";

    /**
     * 缓存管理器,如果没有指定则采用默认的缓存管理器
     */
    @AliasFor(annotation = Cacheable.class, attribute = "cacheManager")
    String cacheManager() default "";

    @AliasFor(annotation = Cacheable.class, attribute = "cacheResolver")
    String cacheResolver() default "";

    /**
     * 判断是否放入缓存的条件,使用Spring EL表达式
     */
    @AliasFor(annotation = Cacheable.class, attribute = "condition")
    String condition() default "";

    /**
     * 在方法执行结束后,根据方法的执行结果执行是否需要放入缓存,例如
     * unless = "#result != null",表示仅当方法执行结果不为null时才放入缓存
     */
    @AliasFor(annotation = Cacheable.class, attribute = "unless")
    String unless() default "";

    /**
     * 是否需要同步调用,如果设置为true,具有相同key的多次调用串行执行
     */
    @AliasFor(annotation = Cacheable.class, attribute = "sync")
    boolean sync() default false;

}
