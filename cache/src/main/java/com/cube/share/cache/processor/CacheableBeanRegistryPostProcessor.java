/*
package com.cube.share.cache.processor;

import com.cube.share.cache.anonotation.ICacheConfig;
import com.cube.share.cache.constant.RedisCacheConstant;
import com.cube.share.cache.writer.IRedisCacheWriter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.cache.RedisCacheWriter;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.lang.NonNull;

import javax.annotation.Resource;
import java.time.Duration;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

*/
/**
 * @author cube.li
 * @date 2021/7/18 16:14
 * @description 支持缓存的Bean后处理器(使用ICacheConfig注解)
 *//*

//@Configuration
@SuppressWarnings("unused")
public class CacheableBeanRegistryPostProcessor implements BeanDefinitionRegistryPostProcessor, ApplicationContextAware {

    private ApplicationContext applicationContext;

    private Set<String> cacheManagerNameSet = new HashSet<>();

    @Resource(type = IRedisCacheWriter.class)
    private IRedisCacheWriter iRedisCacheWriter;

    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
        cacheManagerNameSet.add(RedisCacheConstant.DEFAULT_CACHE_MANAGER_BEAN_NAME);
        //获取所有使用ICacheConfig注解的Bean
        Map<String, Object> annotatedBeanMap = this.applicationContext.getBeansWithAnnotation(ICacheConfig.class);
        //获取所有Bean上的ICacheConfig注解
        Set<Map.Entry<String, Object>> entrySet = annotatedBeanMap.entrySet();
        for (Map.Entry<String, Object> entry : entrySet) {
            Object instance = entry.getValue();
            ICacheConfig iCacheConfig = instance.getClass().getAnnotation(ICacheConfig.class);
            registerRedisCacheManagerBean(registry, iCacheConfig);
        }
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        //do nothing
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    private void registerRedisCacheManagerBean(BeanDefinitionRegistry registry, ICacheConfig annotation) {
        final String cacheManagerName = annotation.cacheManager();
        if (StringUtils.isBlank(cacheManagerName)) {
            return;
        }

        if (!cacheManagerNameSet.contains(cacheManagerName)) {
            RootBeanDefinition definition = new RootBeanDefinition(RedisCacheManager.class);
            MutablePropertyValues propertyValues = new MutablePropertyValues();
            RedisCacheWriter writer = applicationContext.getBean(RedisCacheWriter.class);
            propertyValues.add("cacheWriter", iRedisCacheWriter);
            propertyValues.add("defaultCacheConfig", getRedisCacheConfiguration(annotation));
            definition.setPropertyValues(propertyValues);
            //TODO:事务
            registry.registerBeanDefinition(cacheManagerName, definition);
        }
    }

    @NonNull
    private RedisCacheConfiguration getRedisCacheConfiguration(ICacheConfig annotation) {
        final boolean disableCachingNullValues = annotation.disableCachingNullValues();
        final int expire = annotation.expire();
        final TimeUnit timeUnit = annotation.timeUnit();
        RedisCacheConfiguration config = RedisCacheConfiguration.defaultCacheConfig()
                .serializeKeysWith(RedisSerializationContext.SerializationPair.fromSerializer(new StringRedisSerializer()))
                .serializeValuesWith(RedisSerializationContext.SerializationPair.fromSerializer(new GenericJackson2JsonRedisSerializer()));
        if (disableCachingNullValues) {
            config.disableCachingNullValues();
        }
        if (expire > 0) {
            config.entryTtl(getDuration(expire, timeUnit));
        }
        return config;
    }

    @NonNull
    private RedisCacheManager getRedisCacheManager(ICacheConfig annotation) {
        return RedisCacheManager.RedisCacheManagerBuilder
                .fromCacheWriter(iRedisCacheWriter)
                .transactionAware()
                .cacheDefaults(getRedisCacheConfiguration(annotation))
                .build();
    }

    @NonNull
    private Duration getDuration(int expire, TimeUnit timeUnit) {
        switch (timeUnit) {
            case DAYS:
                return Duration.ofDays(expire);
            case HOURS:
                return Duration.ofHours(expire);
            case MINUTES:
                return Duration.ofMinutes(expire);
            case SECONDS:
                return Duration.ofSeconds(expire);
            case MILLISECONDS:
                return Duration.ofMillis(expire);
            case NANOSECONDS:
                return Duration.ofNanos(expire);
            default:
                throw new IllegalArgumentException("Illegal Redis Cache Expire TimeUnit!");
        }
    }
}
*/
