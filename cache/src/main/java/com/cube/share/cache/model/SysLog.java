package com.cube.share.cache.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author cube.li
 * @date 2021/7/20 23:26
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysLog {

    private Integer id;

    private String operation;
}
