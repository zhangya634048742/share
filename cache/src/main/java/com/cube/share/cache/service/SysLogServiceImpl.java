package com.cube.share.cache.service;

import com.cube.share.cache.anonotation.ICache;
import com.cube.share.cache.anonotation.ICacheConfig;
import com.cube.share.cache.anonotation.ICachePut;
import com.cube.share.cache.model.SysLog;
import org.springframework.stereotype.Service;

/**
 * @author cube.li
 * @date 2021/7/20 23:27
 * @description
 */
@Service
@ICacheConfig(cacheNames = "sysLog", cacheManager = "sysLogCacheManager", expire = 1)
public class SysLogServiceImpl implements SysLogService {

    @Override
    @ICache(key = "#id")
    public SysLog getById(Integer id) {
        return new SysLog(id, "操作" + id);
    }

    @Override
    @ICachePut(key = "#p0.id", condition = "#p0?.id != null")
    public SysLog update(SysLog sysLog) {
        return sysLog;
    }
}
