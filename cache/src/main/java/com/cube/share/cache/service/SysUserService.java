package com.cube.share.cache.service;

import com.cube.share.cache.anonotation.ICache;
import com.cube.share.cache.anonotation.ICacheConfig;
import com.cube.share.cache.anonotation.ICacheEvict;
import com.cube.share.cache.anonotation.ICachePut;
import com.cube.share.cache.model.SysUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author poker.li
 * @date 2021/7/17 15:28
 */
@Service
@ICacheConfig(cacheNames = "sysUser")
@Slf4j
public class SysUserService {

    @ICache(key = "#p0")
    public SysUser getById(Integer id) {
        return new SysUser(id, id + "姓名", id + "地址");
    }

    @ICachePut(key = "#sysUser.id")
    public SysUser update(SysUser sysUser) {
        return sysUser;
    }

    @ICacheEvict(allEntries = true)
    public void deleteById(Integer id) {
        log.debug("删除 {}", id);
    }
}
