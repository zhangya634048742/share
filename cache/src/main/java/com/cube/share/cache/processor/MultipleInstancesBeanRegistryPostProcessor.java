package com.cube.share.cache.processor;

import com.cube.share.cache.model.SysUser;
import org.springframework.beans.BeansException;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.beans.factory.support.RootBeanDefinition;

/**
 * @author poker.li
 * @date 2021/7/19 15:30
 * <p>
 * 测试BeanDefinitionRegistryPostProcessor
 */
//@Configuration
public class MultipleInstancesBeanRegistryPostProcessor implements BeanDefinitionRegistryPostProcessor {

    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
        RootBeanDefinition definition1 = new RootBeanDefinition(SysUser.class);
        MutablePropertyValues user1Values = new MutablePropertyValues();
        user1Values.add("name", "liuwu1");
        user1Values.add("address", "北京");
        definition1.setPropertyValues(user1Values);

        RootBeanDefinition definition2 = new RootBeanDefinition(SysUser.class);
        MutablePropertyValues user2Values = new MutablePropertyValues();
        user2Values.add("name", "zhangsan");
        user2Values.add("address", "上海");
        definition2.setPropertyValues(user2Values);

        registry.registerBeanDefinition("user1", definition1);
        registry.registerBeanDefinition("user2", definition2);
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        //do nothing
    }
}
