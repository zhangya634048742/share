package com.cube.share.cache.anonotation;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * @author cube.li
 * @date 2021/7/17 21:23
 * @description {@link org.springframework.cache.annotation.CacheEvict}提供的缓存清除注解
 */
@SuppressWarnings({"SingleElementAnnotation", "SpringCacheNamesInspection"})
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@CacheEvict
public @interface ICacheEvict {

    /**
     * 缓存的名字(缓存键的前缀),例如,指定为"sysUser",
     * 对于指定的缓存键key="11235813",存在Redis的实际键为 "sysUser::11235813"
     */
    @AliasFor(annotation = CacheEvict.class, attribute = "value")
    String[] value() default {};

    /**
     * 缓存的名字(缓存键的前缀)
     */
    @AliasFor(annotation = CacheEvict.class, attribute = "cacheNames")
    String[] cacheNames() default {};

    /**
     * 缓存键
     */
    @AliasFor(annotation = CacheEvict.class, attribute = "key")
    String key() default "";

    /**
     * 缓存管理器
     */
    @AliasFor(annotation = CacheEvict.class, attribute = "cacheManager")
    String cacheManager() default "";

    @AliasFor(annotation = CacheEvict.class, attribute = "cacheResolver")
    String cacheResolver() default "";

    /**
     * 判断是否放入缓存的条件,使用Spring EL表达式
     */
    @AliasFor(annotation = CacheEvict.class, attribute = "condition")
    String condition() default "";

    /**
     * 是否删除缓存中所有的记录(当前指定的cacheNames下),
     * 如果设置为false,仅删除设定的key
     */
    @AliasFor(annotation = CacheEvict.class, attribute = "allEntries")
    boolean allEntries() default false;

    /**
     * 是否在方法调用前删除缓存,默认是false,仅当方法成功执行后才删除缓存,
     * 如果设定为true,则在调用前即删除缓存,无论方法最终是否调用成功
     */
    @AliasFor(annotation = CacheEvict.class, attribute = "beforeInvocation")
    boolean beforeInvocation() default false;

}
