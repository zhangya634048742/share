package com.cube.share.cache.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author poker.li
 * @date 2021/7/20 14:34
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysDepartment {

    private Integer id;

    private String deptName;

    private String deptAlias;
}
