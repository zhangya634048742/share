package com.cube.share.cache.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author poker.li
 * @date 2021/7/17 15:24
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysUser {

    private Integer id;

    private String name;

    private String address;
}
