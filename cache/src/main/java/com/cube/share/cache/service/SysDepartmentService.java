package com.cube.share.cache.service;

import com.cube.share.cache.anonotation.ICache;
import com.cube.share.cache.anonotation.ICacheConfig;
import com.cube.share.cache.anonotation.ICacheEvict;
import com.cube.share.cache.anonotation.ICachePut;
import com.cube.share.cache.model.SysDepartment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author poker.li
 * @date 2021/7/20 14:35
 */
@Service
@Slf4j
@ICacheConfig(cacheNames = "sysDepartment", cacheManager = "sysDepartmentCacheManager", expire = -1)
public class SysDepartmentService {

    @ICache(key = "#a0")
    public SysDepartment getById(Integer id) {
        return new SysDepartment(id, "部门名字" + id, "部门别名" + id);
    }

    @ICachePut(key = "#p0?.id", condition = "#p0 != null")
    public SysDepartment update(SysDepartment sysDepartment) {
        return sysDepartment;
    }

    @ICacheEvict(key = "#p0")
    public void deleteById(Integer id) {
        log.debug("删除: {}", id);
    }
}
