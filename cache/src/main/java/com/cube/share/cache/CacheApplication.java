package com.cube.share.cache;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author poker.li
 * @date 2021/7/17 14:03
 */
@SpringBootApplication
@ComponentScan("com.cube.share")
public class CacheApplication {

    public static void main(String[] args) {
        SpringApplication.run(CacheApplication.class, args);
    }
}
