package com.cube.share.cache.service;

import com.cube.share.cache.model.SysLog;

/**
 * @author cube.li
 * @date 2021/7/20 23:26
 * @description
 */
public interface SysLogService {

    SysLog getById(Integer id);

    SysLog update(SysLog sysLog);
}
