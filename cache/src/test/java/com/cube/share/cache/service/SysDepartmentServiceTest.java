package com.cube.share.cache.service;

import com.cube.share.cache.model.SysDepartment;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * @author poker.li
 * @date 2021/7/20 14:42
 */
@SpringBootTest
class SysDepartmentServiceTest {

    @Resource
    SysDepartmentService departmentService;

    @Resource
    SysUserService sysUserService;

    @Test
    void getById() {
        sysUserService.getById(1);
        departmentService.getById(1);
    }

    @Test
    void update() {
        departmentService.update(new SysDepartment(1, "更新", "更新"));
    }

    @Test
    void deleteById() {
        departmentService.deleteById(1);
    }
}