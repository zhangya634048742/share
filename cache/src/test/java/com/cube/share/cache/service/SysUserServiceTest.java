package com.cube.share.cache.service;

import com.cube.share.cache.model.SysUser;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * @author poker.li
 * @date 2021/7/17 15:42
 */
@SpringBootTest
class SysUserServiceTest {

    @Resource
    SysUserService sysUserService;

    //@Resource
    SysUser user1;

    //@Resource
    SysUser user2;

    @Test
    void getById() {
        sysUserService.getById(1);
    }

    @Test
    void update() {
        SysUser sysUser = new SysUser(1, "1243名", "134址");
        sysUserService.update(sysUser);
    }

    @Test
    void deleteById() {
        sysUserService.deleteById(2);
    }

    @Test
    void multiBean() {
        System.out.println(user1);
        System.out.println(user2);
    }
}