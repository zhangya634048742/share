package com.cube.share.cache.service;

import com.cube.share.cache.model.SysLog;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * @author cube.li
 * @date 2021/7/20 23:30
 * @description
 */
@SpringBootTest
class SysLogServiceTest {

    @Resource
    SysLogService sysLogService;

    @Test
    void getById() {
        System.out.println(sysLogService.getById(1));
    }

    @Test
    void update() {
        sysLogService.update(new SysLog(1, "更新"));
    }
}