package com.cube.share.delay.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * @author cube.li
 * @date 2021/9/22 16:16
 * @description 基于rabbitmq实现的延时消息配置
 */
@Configuration
public class RabbitMqDelayMessageConfig {

    public static final String DELAYED_QUEUE_NAME = "dm.delayed.queue";

    public static final String DELAYED_EXCHANGE_NAME = "dm.delayed.exchange";

    public static final String DELAYED_ROUTING_KEY = "dm.delayed.routing.key";

    @Bean
    public CustomExchange delayedExchange() {
        Map<String, Object> args = new HashMap<>(2);
        args.put("x-delayed-type", "direct");
        return new CustomExchange(DELAYED_EXCHANGE_NAME, "x-delayed-message", true, false, args);
    }

    @Bean
    public Queue delayedQueue() {
        return QueueBuilder.durable(DELAYED_QUEUE_NAME).build();
    }

    @Bean
    public Binding delayedQueueBindingExchange() {
        return BindingBuilder.bind(delayedQueue()).to(delayedExchange()).with(DELAYED_ROUTING_KEY).noargs();
    }
}
