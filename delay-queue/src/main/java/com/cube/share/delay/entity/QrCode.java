package com.cube.share.delay.entity;

import lombok.Data;

/**
 * @author cube.li
 * @date 2021/9/22 16:11
 * @description 二维码
 */
@Data
public class QrCode {

    private String url;

    private String configId;
}
