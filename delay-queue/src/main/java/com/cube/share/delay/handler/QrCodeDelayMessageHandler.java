package com.cube.share.delay.handler;

import com.cube.share.base.utils.JacksonUtils;
import com.cube.share.delay.entity.QrCode;
import com.cube.share.delay.message.DelayMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author cube.li
 * @date 2021/9/22 16:08
 * @description 二维码延时消息处理器
 */
@Component
@Slf4j
public class QrCodeDelayMessageHandler implements DelayMessageHandler {

    @Override
    public void handle(DelayMessage message) {
        log.info("二维码延时消息处理中,message = {}", message.toString());
        QrCode qrCode = JacksonUtils.readJsonString(message.getBody(), QrCode.class);
        //删除二维码
    }
}
