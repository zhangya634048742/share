package com.cube.share.delay.entity;

import lombok.Data;

/**
 * @author cube.li
 * @date 2021/9/22 18:01
 * @description 任务
 */
@Data
public class ExecuteTask {

    private Long id;
}
