package com.cube.share.delay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author cube.li
 * @date 2021/9/22 15:24
 * @description 启动类
 */
@SpringBootApplication(scanBasePackages = "com.cube.share")
public class DelayQueueApplication {

    public static void main(String[] args) {
        SpringApplication.run(DelayQueueApplication.class, args);
    }
}
