package com.cube.share.delay.manager;

import cn.hutool.core.thread.ThreadUtil;
import com.cube.share.delay.handler.DelayMessageHandler;
import com.cube.share.delay.message.DelayMessage;
import com.cube.share.delay.message.DelayMessageType;
import lombok.extern.slf4j.Slf4j;
import org.redisson.Redisson;
import org.redisson.api.RBlockingQueue;
import org.redisson.api.RDelayedQueue;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static com.cube.share.base.utils.SpringContextUtil.getBean;

/**
 * @author cube.li
 * @date 2021/10/11 20:59
 * @description 基于redisson实现的延时消息管理器
 */
@Component
@Slf4j
public class RedissonDelayMessageManager implements DelayMessageManager {

    private RedissonClient redissonClient = Redisson.create();

    private RBlockingQueue<DelayMessage> rBlockingQueue = redissonClient.getBlockingDeque("redisson-delay-message-queue");

    private RDelayedQueue<DelayMessage> rDelayedQueue = redissonClient.getDelayedQueue(rBlockingQueue);

    private final Map<DelayMessageType, DelayMessageHandler> handlerMap = new ConcurrentHashMap<>(16);

    @Override
    public void add(DelayMessage message) {
        if (rDelayedQueue.contains(message)) {
            return;
        }
        log.info("redisson-delay-message-queue,add message = {}", message);
        rDelayedQueue.offer(message, message.getProperties().getExpire(), message.getProperties().getTimeUnit());
    }

    @Override
    public boolean remove(DelayMessage message) {
        return rDelayedQueue.remove(message);
    }

    @Override
    public void destroy() {
        rDelayedQueue.destroy();
    }

    @Override
    public void afterPropertiesSet() {
        Arrays.stream(DelayMessageType.values()).forEach(delayMessageType -> handlerMap.put(delayMessageType, getBean(delayMessageType.getHandler())));
        Thread thread = new Thread(() -> {
            while (true) {
                try {
                    DelayMessage delayMessage = rBlockingQueue.take();
                    log.info("redisson-delay-message-queue,consume message = {}", delayMessage);
                    handlerMap.get(delayMessage.getType()).handle(delayMessage);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    ThreadUtil.safeSleep(1000);
                    afterPropertiesSet();
                }
            }
        });
        thread.setDaemon(true);
        thread.start();
    }
}
