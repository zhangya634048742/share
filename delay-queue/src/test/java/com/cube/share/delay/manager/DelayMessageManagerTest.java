package com.cube.share.delay.manager;

import com.cube.share.base.utils.JacksonUtils;
import com.cube.share.delay.entity.ExecuteTask;
import com.cube.share.delay.entity.QrCode;
import com.cube.share.delay.message.DelayMessage;
import com.cube.share.delay.message.DelayMessageType;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * @author cube.li
 * @date 2021/9/22 17:12
 * @description 测试
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class DelayMessageManagerTest {

    @Resource(type = RedissonDelayMessageManager.class)
    DelayMessageManager delayMessageManager;

    @Test
    void add() throws JsonProcessingException {
        DelayMessage delayMessage = new DelayMessage();
        QrCode qrCode = new QrCode();
        qrCode.setConfigId("fadfdaf110");
        qrCode.setUrl("http://www.baidu.com");
        delayMessage.setBody(JacksonUtils.toJsonString(qrCode));
        delayMessage.setType(DelayMessageType.DELETE_QR_CODE);
        DelayMessage.DelayMessageProperties properties = new DelayMessage.DelayMessageProperties();
        properties.setExpire(10);
        properties.setTimeUnit(TimeUnit.SECONDS);
        delayMessage.setProperties(properties);
        delayMessageManager.add(delayMessage);


        DelayMessage delayMessage1 = new DelayMessage();
        QrCode qrCode1 = new QrCode();
        qrCode1.setConfigId("fadfdaf1405");
        qrCode1.setUrl("http://www.baidu.com");
        delayMessage1.setBody(JacksonUtils.toJsonString(qrCode1));
        delayMessage1.setType(DelayMessageType.DELETE_QR_CODE);
        DelayMessage.DelayMessageProperties properties1 = new DelayMessage.DelayMessageProperties();
        properties1.setExpire(5);
        properties1.setTimeUnit(TimeUnit.SECONDS);
        delayMessage1.setProperties(properties1);
        delayMessageManager.add(delayMessage1);

        DelayMessage delayMessage2 = new DelayMessage();
        ExecuteTask task = new ExecuteTask();
        task.setId(1L);
        delayMessage2.setBody(JacksonUtils.toJsonString(task));
        delayMessage2.setType(DelayMessageType.EXECUTE_TASK);

        DelayMessage.DelayMessageProperties properties2 = new DelayMessage.DelayMessageProperties();
        properties2.setExpire(9);
        properties2.setTimeUnit(TimeUnit.SECONDS);
        delayMessage2.setProperties(properties2);
        delayMessage2.setSerialId(11235813L);
        delayMessageManager.add(delayMessage2);
        //delayMessageManager.remove(delayMessage2);

    }
}