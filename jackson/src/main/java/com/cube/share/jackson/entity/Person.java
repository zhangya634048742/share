package com.cube.share.jackson.entity;

import com.cube.share.jackson.annotation.SensitiveData;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author poker.li
 * @date 2021/8/16 14:16
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Person {

    private Long id;

    private String name;

    private String address;

    private Integer age;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createAt;

    private LocalDateTime updateAt;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate lastLoginDate;

    @SensitiveData(strategy = SensitiveData.Strategy.LEFT, length = 6, replaceStr = "*")
    private String mobile;

    @JsonUnwrapped
    private Role role;
}
