package com.cube.share.jackson.annotation;

import com.cube.share.jackson.serializer.SensitiveDataSerializer;
import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.lang.annotation.*;

/**
 * @author poker.li
 * @date 2021/8/16 15:44
 * <p>
 * 需要脱密的字段注解
 */
@Retention(RetentionPolicy.RUNTIME)
@Documented
@JacksonAnnotationsInside
@Target(ElementType.FIELD)
@JsonSerialize(using = SensitiveDataSerializer.class)
public @interface SensitiveData {

    /**
     * 默认的字段脱敏替换字符串
     */
    String DEFAULT_REPLACE_STRING = "*";

    /**
     * 脱敏策略
     */
    Strategy strategy() default Strategy.TOTAL;

    /**
     * 脱敏长度,在Strategy.TOTAL策略下忽略该字段
     */
    int length() default 0;

    /**
     * 脱敏字段替换字符
     */
    String replaceStr() default DEFAULT_REPLACE_STRING;

    enum Strategy {
        /**
         * 全部
         */
        TOTAL,
        /**
         * 从左边开始
         */
        LEFT,
        /**
         * 从右边开始
         */
        RIGHT
    }
}
