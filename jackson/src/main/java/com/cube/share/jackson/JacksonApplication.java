package com.cube.share.jackson;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author poker.li
 * @date 2021/8/16 14:09
 * <p>
 * 启动类
 */
@SpringBootApplication
@ComponentScan("com.cube.share")
public class JacksonApplication {

    public static void main(String[] args) {
        SpringApplication.run(JacksonApplication.class, args);
    }
}
