package com.cube.share.jackson.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @author poker.li
 * @date 2021/8/18 9:59
 */
@Data
public class Role {

    @JsonProperty("roleId")
    private Long id;

    private String roleName;

    private String desc;
}
