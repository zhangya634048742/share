package com.cube.share.jackson.annotation;

import com.cube.share.jackson.deserializer.SearchEscapeDeserializer;
import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.lang.annotation.*;

/**
 * @author poker.li
 * @date 2021/10/26 14:04
 * <p>
 * 搜索内容转义注解,将使用该注解标注的属性(字符串)中指定的字符转义
 */
@Retention(RetentionPolicy.RUNTIME)
@Documented
@JacksonAnnotationsInside
@Target(ElementType.FIELD)
@JsonDeserialize(using = SearchEscapeDeserializer.class)
public @interface SearchEscape {

    /**
     * mysql通配符正则
     */
    String MYSQL_WILDCARD_REGEX = "[%|_]";

    /**
     * 需要转义的字符数组
     */
    char[] needsToEscape() default {'%', '_'};

}
