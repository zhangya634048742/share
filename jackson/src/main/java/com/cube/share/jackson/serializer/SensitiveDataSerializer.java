package com.cube.share.jackson.serializer;

import com.cube.share.jackson.annotation.SensitiveData;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;

/**
 * @author poker.li
 * @date 2021/8/16 19:32
 * <p>
 * 脱敏字段序列化器
 */
public class SensitiveDataSerializer extends JsonSerializer<String> implements ContextualSerializer {

    private SensitiveData sensitiveData;

    public SensitiveDataSerializer(SensitiveData sensitiveData) {
        this.sensitiveData = sensitiveData;
    }

    public SensitiveDataSerializer() {
    }


    @Override
    public void serialize(String value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        if (StringUtils.isBlank(value)) {
            gen.writeString(value);
            return;
        }

        if (sensitiveData != null) {
            final SensitiveData.Strategy strategy = sensitiveData.strategy();
            final int length = sensitiveData.length();
            final String replaceString = sensitiveData.replaceStr();
            gen.writeString(getValue(value, strategy, length, replaceString));
        } else {
            gen.writeString(value);
        }

    }

    @Override
    public JsonSerializer<?> createContextual(SerializerProvider prov, BeanProperty property) {
        SensitiveData annotation = property.getAnnotation(SensitiveData.class);

        if (annotation != null) {
            return new SensitiveDataSerializer(annotation);
        }
        return this;
    }

    private String getValue(String rawStr, SensitiveData.Strategy strategy, int length, String replaceString) {
        switch (strategy) {
            case TOTAL:
                return rawStr.replaceAll("[\\s\\S]", replaceString);
            case LEFT:
                return replaceByLength(rawStr, length, replaceString, true);
            case RIGHT:
                return replaceByLength(rawStr, length, replaceString, false);
            default:
                throw new IllegalArgumentException("Illegal Sensitive Strategy");
        }
    }

    private String replaceByLength(String rawStr, int length, String replaceString, boolean fromLeft) {
        if (StringUtils.isBlank(rawStr)) {
            return rawStr;
        }
        if (rawStr.length() <= length) {
            return rawStr.replaceAll("[\\s\\S]", replaceString);
        }

        if (fromLeft) {
            return getSpecStringSequence(length, replaceString) + rawStr.substring(length);
        } else {
            return rawStr.substring(0, length) + getSpecStringSequence(length, replaceString);
        }
    }

    private String getSpecStringSequence(int length, String str) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < length; i++) {
            stringBuilder.append(str);
        }
        return stringBuilder.toString();
    }
}
