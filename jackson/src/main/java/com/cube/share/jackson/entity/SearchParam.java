package com.cube.share.jackson.entity;

import com.cube.share.jackson.annotation.SearchEscape;
import lombok.Data;

/**
 * @author poker.li
 * @date 2021/10/26 15:29
 */
@Data
public class SearchParam {

    @SearchEscape
    private String name;

    private String address;
}
