package com.cube.share.jackson.config;

import com.cube.share.jackson.factory.SearchEscapeFormatterFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * @author poker.li
 * @date 2021/10/26 16:30
 */
@Configuration
@Slf4j
public class MvcConfigure implements WebMvcConfigurer {

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
        resolvers.add(new SensitiveDataArgumentResolver());
        WebMvcConfigurer.super.addArgumentResolvers(resolvers);
    }

    @Override
    public void addFormatters(FormatterRegistry registry) {
        log.info("@SearchEscape注解");
        registry.addFormatterForFieldAnnotation(SearchEscapeFormatterFactory.INSTANCE);
    }
}
