package com.cube.share.jackson.controller;

import com.cube.share.base.templates.ApiResult;
import com.cube.share.jackson.entity.Person;
import com.cube.share.jackson.entity.Role;
import com.cube.share.jackson.entity.SearchParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author poker.li
 * @date 2021/8/16 14:17
 * <p>
 * 参考 {@link <a href="https://segmentfault.com/a/1190000020885579">...</a>}
 */
@RestController
@RequestMapping("/person")
@Slf4j
public class PersonController {

    @GetMapping("/detail/{id}")
    public ApiResult detail(@PathVariable("id") Long id) {
        Role role = new Role();
        role.setId(12L);
        role.setRoleName("管理员");
        return ApiResult.success(Person.builder()
                .id(id)
                .age(18)
                .name("lis")
                .address("北京")
                .createAt(LocalDateTime.now())
                .updateAt(LocalDateTime.now())
                .lastLoginDate(LocalDate.now())
                .mobile("15824984456")
                .role(role)
                .build());
    }

    @PostMapping("/save")
    public ApiResult<Void> save(@RequestBody Person person) {
        log.info("用户信息: {}", person);
        return ApiResult.success();
    }

    @PostMapping("/search")
    public ApiResult<SearchParam> search(@RequestBody SearchParam param) {
        log.info(param.toString());
        return ApiResult.success(param);
    }

    @GetMapping("/page")
    public ApiResult<Void> page(@Valid @Min(value = 0L, message = "页数必须大于等于0") Integer page,
                                @Valid @Min(value = 0L, message = "每页大小必须大于等于0") Integer pageSize) {
        return ApiResult.success();
    }
}
