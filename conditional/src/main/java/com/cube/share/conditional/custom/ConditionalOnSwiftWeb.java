package com.cube.share.conditional.custom;

import org.springframework.context.annotation.Conditional;

import java.lang.annotation.*;

/**
 * @author litb
 * @date 2022/3/15 9:56
 * <p>
 * 是否是Swift框架Web项目l
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Conditional(OnSwiftWebCondition.class)
public @interface ConditionalOnSwiftWeb {
}
