package com.cube.share.conditional.custom;

import org.springframework.context.annotation.Conditional;

import java.lang.annotation.*;

/**
 * @author litb
 * @date 2022/3/15 10:05
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Conditional(OnNonSwiftWebCondition.class)
public @interface ConditionalOnNonSwiftWeb {
}
