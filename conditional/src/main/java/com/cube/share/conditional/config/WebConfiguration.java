package com.cube.share.conditional.config;

import com.cube.share.conditional.custom.ConditionalOnNonSwiftWeb;
import com.cube.share.conditional.custom.ConditionalOnSwiftWeb;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author litb
 * @date 2022/3/15 10:03
 */
@Configuration
//@ConditionalOnSwiftWeb
//@ConditionalOnNonSwiftWeb
public class WebConfiguration {

    @Bean
    @ConditionalOnSwiftWeb
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
