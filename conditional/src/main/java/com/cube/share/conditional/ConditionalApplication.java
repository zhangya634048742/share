package com.cube.share.conditional;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author litb
 * @date 2022/3/15 9:53
 */
@SpringBootApplication
public class ConditionalApplication {

    static {
        System.setProperty("project_id", "web_123");
    }

    public static void main(String[] args) {
        SpringApplication.run(ConditionalApplication.class, args);
    }
}
