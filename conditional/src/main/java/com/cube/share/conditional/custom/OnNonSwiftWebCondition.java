package com.cube.share.conditional.custom;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;
import org.springframework.lang.NonNull;

/**
 * @author litb
 * @date 2022/3/15 10:06
 */
public class OnNonSwiftWebCondition implements Condition {
    @Override
    public boolean matches(@NonNull ConditionContext context, @NonNull AnnotatedTypeMetadata metadata) {
        String projectId = System.getProperty("project_id");
        return !StringUtils.isNotBlank(projectId) || !projectId.startsWith("web");
    }
}
