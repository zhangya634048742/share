package com.cube.share.async.controller;

import com.cube.share.async.executor.AsyncExecutor;
import com.cube.share.base.templates.ApiResult;
import com.cube.share.base.templates.CustomException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.concurrent.ExecutionException;

/**
 * @author cube.li
 * @date 2022/2/19 17:29
 */
@RestController
public class AsyncController {

    @Resource
    private AsyncExecutor asyncExecutor;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @GetMapping("/async")
    public ApiResult<Integer> async() throws ExecutionException, InterruptedException {
        asyncExecutor.execute(() -> {
            throw new CustomException("异常发生了");
        });
       /* Future<Integer> future = asyncExecutor.executeWithResult(() -> {
           throw new RuntimeException("error");
        });*/
        threadPoolTaskExecutor.submit(() -> System.out.println("spring task thread execute..."));
        return ApiResult.success(null);
    }
}
