package com.cube.share.async.thread;

/**
 * @author cube.li
 * @date 2022/2/19 14:47
 * <p>
 * 线程装饰器,在线程执行时对其进行增强(执行耗时统计、异常处理、清除ThreadLocal等)
 */
@FunctionalInterface
public interface ThreadDecorator {

    /**
     * 装饰给定的runnable,返回一个包装后的runnable用于实际执行
     * @param runnable runnable
     * @return 包装后的runnable
     */
    Runnable decorate(Runnable runnable);
}
