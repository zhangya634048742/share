package com.cube.share.spi.service;

import java.io.InputStream;

/**
 * @author cube.li
 * @date 2021/7/7 22:39
 * @description 基于oss实现的文件服务
 */
public class OssFileService implements FileService {

    @Override
    public Object upload(InputStream inputStream) {
        return "OSS 文件上传";
    }

    @Override
    public void download(String fileId, String path) {
        System.out.println("OSS 文件下载...");
    }

    @Override
    public String getType() {
        return "OSS";
    }
}
