package com.cube.share.spi.service;

import java.io.InputStream;

/**
 * @author cube.li
 * @date 2021/7/7 22:25
 * @description 文件服务
 */
public interface FileService {

    /**
     * 文件上传
     *
     * @param inputStream inputStream
     * @return 上传后的文件信息
     */
    Object upload(InputStream inputStream);

    /**
     * 文件下载
     *
     * @param fileId 文件id
     * @param path   文件下载路径
     */
    void download(String fileId, String path);

    /**
     * 获取对象存储类型
     * @return 存储类型
     */
    String getType();
}
