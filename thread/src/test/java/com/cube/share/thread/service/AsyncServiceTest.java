package com.cube.share.thread.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author litb
 * @date 2022/2/18 23:23
 */
@SpringBootTest
class AsyncServiceTest {

    @Autowired
    AsyncService asyncService;

    @Test
    void doAction() throws Exception {
        asyncService.doAction();
    }
}