package com.cube.share.thread.controller;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author poker.li
 * @date 2021/7/31 15:21
 */
@SpringBootTest
class JustControllerTest {

    @Resource
    JustController justController;

    @Test
    void request() {
        justController.request();
    }
}