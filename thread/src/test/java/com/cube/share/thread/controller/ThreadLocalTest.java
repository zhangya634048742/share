package com.cube.share.thread.controller;

import com.cube.share.thread.entity.SysUser;
import org.junit.Test;

import java.lang.reflect.Field;

/**
 * @author cube.li
 * @date 2021/11/21 12:49
 * @description 测试
 */
public class ThreadLocalTest {

    @Test
    public void loop() throws Exception {

        for (int i = 0; i < 1; i++) {
            ThreadLocal<SysUser> threadLocal = new ThreadLocal<>();
            threadLocal.set(new SysUser(System.currentTimeMillis(), "李四"));
            threadLocal = null;
            System.gc();
            printEntryInfo();
        }

        //System.gc();

        //printEntryInfo();
    }

    private void printEntryInfo() throws Exception {
        Thread currentThread = Thread.currentThread();
        Class<? extends Thread> clz = currentThread.getClass();
        Field field = clz.getDeclaredField("threadLocals");
        field.setAccessible(true);
        Object threadLocalMap = field.get(currentThread);
        Class<?> tlmClass = threadLocalMap.getClass();
        Field tableField = tlmClass.getDeclaredField("table");
        tableField.setAccessible(true);
        Object[] arr = (Object[]) tableField.get(threadLocalMap);
        for (Object o : arr) {
            if (o != null) {
                Class<?> entryClass = o.getClass();
                Field valueField = entryClass.getDeclaredField("value");
                Field referenceField = entryClass.getSuperclass().getSuperclass().getDeclaredField("referent");
                valueField.setAccessible(true);
                referenceField.setAccessible(true);
                System.out.println(String.format("弱引用key:%s,值:%s", referenceField.get(o), valueField.get(o)));
            }
        }
    }

    private static final ThreadLocal<String> RESOURCE = new ThreadLocal<>();

    @Test
    public void multiThread() {
        Thread thread1 = new Thread(() -> {
            RESOURCE.set("thread1");
            System.gc();
            try {
                printEntryInfo();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        Thread thread2 = new Thread(() -> {
            //RESOURCE.set("thread2");
            //System.gc();
            try {
                printEntryInfo();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        thread1.start();
        thread2.start();
    }


}
