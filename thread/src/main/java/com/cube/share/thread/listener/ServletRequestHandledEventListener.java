package com.cube.share.thread.listener;

import com.cube.share.thread.config.CurrentUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.ServletRequestHandledEvent;

/**
 * @author poker.li
 * @date 2021/7/31 15:12
 * <p>
 * ServletRequest请求监听器
 */
@Component
@Slf4j
public class ServletRequestHandledEventListener implements ApplicationListener<ServletRequestHandledEvent> {

    @Override
    public void onApplicationEvent(ServletRequestHandledEvent event) {
        CurrentUser.removeAll();
        log.debug("清除当前线程用户信息,uri = {},method = {},servletName = {},clientAddress = {}", event.getRequestUrl(),
                event.getMethod(), event.getServletName(), event.getClientAddress());
    }
}
