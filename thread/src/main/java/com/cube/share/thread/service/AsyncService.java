package com.cube.share.thread.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author litb
 * @date 2022/2/18 23:21
 */
@Service
public class AsyncService {

    @Autowired
    private ThreadPoolExecutor customExecutor;

    public void doAction() throws Exception {
        Future<String> future1 = customExecutor.submit(() -> {
            try {
                Thread.sleep(2500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("业务逻辑1执行中");
            return "hello";
        });

        Future<Integer> future2 = customExecutor.submit(() -> {
            Thread.sleep(1500);
            System.out.println("业务逻辑2执行中");
            throw new RuntimeException("异常");
            //return 12;
        });
        System.out.println(future2.get());
        System.out.println(future1.get());
    }
}
