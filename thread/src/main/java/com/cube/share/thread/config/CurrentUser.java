package com.cube.share.thread.config;

import com.cube.share.thread.entity.SysUser;

/**
 * @author poker.li
 * @date 2021/7/31 14:50
 * <p>
 * 线程当前用户信息
 */
public class CurrentUser {

    private static final ThreadLocal<SysUser> USER = new ThreadLocal<>();

    private static final ThreadLocal<Long> USER_ID = new ThreadLocal<>();

    private static final InheritableThreadLocal<SysUser> INHERITABLE_USER = new InheritableThreadLocal<>();

    private static final InheritableThreadLocal<Long> INHERITABLE_USER_ID = new InheritableThreadLocal<>();

    public static void setUser(SysUser sysUser) {
        USER.set(sysUser);
        INHERITABLE_USER.set(sysUser);
    }

    public static void setUserId(Long id) {
        USER_ID.set(id);
        INHERITABLE_USER_ID.set(id);
    }

    public static SysUser user() {
        return USER.get();
    }

    public static SysUser inheritableUser() {
        return INHERITABLE_USER.get();
    }

    public static Long inheritableUserId() {
        return INHERITABLE_USER_ID.get();
    }

    public static Long userId() {
        return USER_ID.get();
    }

    public static void removeAll() {
        USER.remove();
        USER_ID.remove();
        INHERITABLE_USER.remove();
        INHERITABLE_USER_ID.remove();
    }
}
