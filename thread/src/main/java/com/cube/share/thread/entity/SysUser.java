package com.cube.share.thread.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author poker.li
 * @date 2021/7/31 14:51
 * <p>
 * 系统用户
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysUser {

    private Long id;

    private String name;
}
