package com.cube.share.thread.service;

import com.cube.share.thread.config.CurrentUser;
import org.springframework.stereotype.Service;

/**
 * @author poker.li
 * @date 2021/7/31 15:07
 */
@Service
public class JustService {

    /**
     * 模拟业务执行
     */
    public void doAction() {
        //在业务执行时获取当前线程中的用户信息
        System.out.println("当前线程用户信息:" + CurrentUser.user());
    }
}
