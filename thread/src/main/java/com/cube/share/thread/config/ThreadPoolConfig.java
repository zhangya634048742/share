package com.cube.share.thread.config;

import cn.hutool.core.thread.ThreadFactoryBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author poker.li
 * @date 2021/3/16 14:10
 */
@Configuration
@Slf4j
public class ThreadPoolConfig {

    /**
     * 阻塞系数
     */
    @Value("${ewArchiveExecutor.blockageCoefficient:0.5f}")
    private float blockageCoefficient;

    /**
     * 线程池
     */
    @Bean
    public ThreadPoolExecutor customExecutor() {
        return new ThreadPoolExecutor((int) (Runtime.getRuntime().availableProcessors() / blockageCoefficient),
                (int) (Runtime.getRuntime().availableProcessors() / blockageCoefficient + 10),
                30, TimeUnit.SECONDS, new ArrayBlockingQueue<>(30),
                ThreadFactoryBuilder.create().setNamePrefix("share-thread-pool:thread-d%").build(), new ThreadPoolExecutor.CallerRunsPolicy());
    }
}
