package com.cube.share.resubmit.check.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author cube.li
 * @date 2021/7/9 23:06
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Person {

    private Integer id;

    private String name;

    private String address;
}
