package com.cube.share.resubmit.check.aspect;

import com.cube.share.base.constants.Constant;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * @author cube.li
 * @date 2021/7/9 20:45
 * @description 防重复提交注解
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RepeatSubmitCheck {

    /**
     * 参数Spring EL表达式例如 #{param.name},表达式的值作为防重复校验key的一部分
     */
    String[] argExpressions();

    /**
     * 重复提交报错信息
     */
    String message() default Constant.RESUBMIT_MSG;

    /**
     * Spring EL表达式,决定是否进行重复提交校验,多个条件之间为且的关系,默认是进行校验
     */
    String[] conditionExpressions() default {"true"};

    /**
     * 是否将方法信息作为key的一部分,这里的方法信息为
     * 类名 + '_'  + 方法名
     */
    boolean withMethodInfo() default true;

    /**
     * 是否选用当前操作用户的信息作为防重复提交校验key的一部分
     */
    boolean withUserInfoInKey() default true;

    /**
     * 是否仅在当前session内进行防重复提交校验
     */
    boolean onlyInCurrentSession() default false;

    /**
     * 防重复提交校验的时间间隔
     */
    long interval() default 1;

    /**
     * 防重复提交校验的时间间隔的单位
     */
    TimeUnit timeUnit() default TimeUnit.SECONDS;


}
