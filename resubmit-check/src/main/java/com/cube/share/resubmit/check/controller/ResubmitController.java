package com.cube.share.resubmit.check.controller;

import com.cube.share.base.templates.ApiResult;
import com.cube.share.resubmit.check.aspect.RepeatSubmitCheck;
import com.cube.share.resubmit.check.model.Person;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author cube.li
 * @date 2021/7/9 23:05
 * @description
 */
@RestController
public class ResubmitController {

    @PostMapping("/save")
    @RepeatSubmitCheck(argExpressions = {"#person.id", "#person.name"}, conditionExpressions = "[0].address != null")
    public ApiResult<Void> save(@RequestBody Person person) {
        return ApiResult.success();
    }
}
