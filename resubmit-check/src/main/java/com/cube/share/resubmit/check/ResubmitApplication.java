package com.cube.share.resubmit.check;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author cube.li
 * @date 2021/7/9 23:04
 * @description 启动类
 */
@SpringBootApplication
@ComponentScan("com.cube.share")
public class ResubmitApplication {

    public static void main(String[] args) {
        SpringApplication.run(ResubmitApplication.class, args);
    }
}
