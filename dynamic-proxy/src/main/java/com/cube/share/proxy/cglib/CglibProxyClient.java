package com.cube.share.proxy.cglib;

import org.springframework.cglib.proxy.Enhancer;

/**
 * @author cube.li
 * @date 2021/3/6 20:33
 * @description
 */
public class CglibProxyClient {

    public static void main(String[] args) {
        //目标对象
        Runner runner = new Runner("李四");
        //代理对象
        RunnerMethodInterceptor interceptor = new RunnerMethodInterceptor(runner);
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(runner.getClass());
        //设置回调
        enhancer.setCallback(interceptor);
        Runner proxyInstance = (Runner) enhancer.create();
        proxyInstance.run();
    }
}
