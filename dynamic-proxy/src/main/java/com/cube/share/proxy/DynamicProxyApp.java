package com.cube.share.proxy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author cube.li
 * @date 2021/3/6 13:15
 * @description 启动类
 */
@SpringBootApplication
public class DynamicProxyApp {

    public static void main(String[] args) {
        SpringApplication.run(DynamicProxyApp.class, args);
    }
}
