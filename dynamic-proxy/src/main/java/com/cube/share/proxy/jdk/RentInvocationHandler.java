package com.cube.share.proxy.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @author cube.li
 * @date 2021/3/6 18:24
 * @description
 */
public class RentInvocationHandler implements InvocationHandler {

    /**
     * 被代理的目标对象
     */
    private Object target;

    public RentInvocationHandler(Object target) {
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("租房前预处理工作...");
        Object returnValue = method.invoke(target, args);
        System.out.println("租房后善后工作...");
        return returnValue;
    }
}
