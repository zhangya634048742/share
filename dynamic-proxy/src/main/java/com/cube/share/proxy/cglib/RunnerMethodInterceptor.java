package com.cube.share.proxy.cglib;

import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @author cube.li
 * @date 2021/3/6 20:37
 * @description
 */
public class RunnerMethodInterceptor implements MethodInterceptor {

    /**
     * 目标对象
     */
    private Object target;

    public RunnerMethodInterceptor(Object target) {
        this.target = target;
    }

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        System.out.println("踢两脚 加速...");
        Object returnVal = method.invoke(target, objects);
        System.out.println("拽两下 减速...");
        return returnVal;
    }
}
