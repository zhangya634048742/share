package com.cube.share.proxy.stat;

/**
 * @author cube.li
 * @date 2021/3/6 14:32
 * @description 中介
 */
public class Intermediary implements Person {

    private String name;

    /**
     * 租房者
     */
    private Person tenant;

    public Intermediary(String name, Person tenant) {
        this.name = name;
        this.tenant = tenant;
    }

    @Override
    public void rentHouse() {
        System.out.println("中介 " + this.name + "确定租房预算,带领查看房源...");
        tenant.rentHouse();
        System.out.println("中介 " + this.name + "带领客户租房完成,合同签订完成,收取佣金,准备跑路...");
    }
}
