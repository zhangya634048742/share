package com.cube.share.proxy.stat;

/**
 * @author cube.li
 * @date 2021/3/6 14:25
 * @description 接口
 */
public interface Person {

    /**
     * 租房
     */
    void rentHouse();
}
