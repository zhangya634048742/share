package com.cube.share.proxy.stat;

/**
 * @author cube.li
 * @date 2021/3/6 14:40
 * @description
 */
public class Client {

    public static void main(String[] args) {
        Person tenantLiSi = new Tenant("李四");
        Person intermediaryWangWu = new Intermediary("王五", tenantLiSi);
        //代理模式完成租房
        intermediaryWangWu.rentHouse();
        System.out.println("-----------------");
        Person tenantZs = new Tenant("张三");
        Person intermediaryChenDa = new Intermediary("陈大", tenantZs);
        intermediaryChenDa.rentHouse();
    }
}
