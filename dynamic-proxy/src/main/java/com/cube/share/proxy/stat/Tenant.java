package com.cube.share.proxy.stat;

/**
 * @author cube.li
 * @date 2021/3/6 14:28
 * @description 租房者
 */
public class Tenant implements Person {

    private String name;

    public Tenant(String name) {
        this.name = name;
    }

    @Override
    public void rentHouse() {
        System.out.println(this.name + " 正在租房中...");
    }
}
