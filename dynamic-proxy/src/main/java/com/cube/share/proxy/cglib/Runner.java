package com.cube.share.proxy.cglib;

/**
 * @author cube.li
 * @date 2021/3/6 20:34
 * @description 奔跑者
 */
public class Runner {

    private String name;

    public Runner() {
    }

    public Runner(String name) {
        this.name = name;
    }

    public void run() {
        System.out.println(this.name + " 跑的飞快...");
    }
}
