package com.cube.share.qrcode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author cube.li
 * @date 2021/3/28 13:59
 * @description 启动类
 */
@SpringBootApplication
public class QRCodeApplication {

    public static void main(String[] args) {
        SpringApplication.run(QRCodeApplication.class, args);
    }
}
