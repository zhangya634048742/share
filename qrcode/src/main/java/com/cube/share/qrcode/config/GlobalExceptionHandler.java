package com.cube.share.qrcode.config;

import com.cube.share.base.templates.ApiResult;
import com.cube.share.base.templates.CustomException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author litb
 * @date 2021/3/11 15:21
 * @description 全局异常处理器
 */
@ResponseBody
@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(CustomException.class)
    public ApiResult customExceptionHandler(CustomException ce) {
        return ApiResult.error(ce.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public ApiResult exceptionHandler(Exception e) {
        log.error("系统异常,error_msg = {}", e.getMessage());
        return ApiResult.error("系统异常!");
    }
}
