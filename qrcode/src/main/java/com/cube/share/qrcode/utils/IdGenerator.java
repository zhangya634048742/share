package com.cube.share.qrcode.utils;

import lombok.extern.slf4j.Slf4j;

import java.util.UUID;

/**
 * @author cube.li
 * @date 2021/3/28 14:45
 * @description 主键id生成器
 */
@Slf4j
public class IdGenerator {

    public static String generatorStringId() {
        String id = UUID.randomUUID().toString().replace("-", "");
        log.info("generate String Id, id = {}", id);
        return id;
    }

}
