package com.cube.share.qrcode.controller;

import com.cube.share.base.templates.ApiResult;
import com.cube.share.base.utils.IAssert;
import com.cube.share.qrcode.mock.MockData;
import com.cube.share.qrcode.service.MockDataService;
import com.cube.share.qrcode.utils.QRCodeUtil;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.awt.image.BufferedImage;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * @author cube.li
 * @date 2021/3/28 15:02
 * @description
 */
@RestController
@RequestMapping("/data")
@Slf4j
public class MockController {

    @Resource
    private MockDataService dataService;

    @Resource
    private HttpServletResponse response;

    private static final String QR_CODE_SCAN_URL = "localhost:8080/data/scan?id=";

    @Value("${cube.qr-code-scan-url}")
    private String qrCodeScanUrl;

    @PostMapping("/save")
    public ApiResult save(@RequestBody @Valid MockData data) {
        dataService.save(data);
        return ApiResult.success();
    }

    @GetMapping("/qrCode")
    public void qrCode(@RequestParam("id") String id) throws Exception {
        MockData data = dataService.get(id);
        IAssert.notNull(data, "未查找到对应数据");
        //将二维码图片响应到前端
        BufferedImage image = QRCodeUtil.createQrCodeImage(qrCodeScanUrl + id, 256, 256);
        //BufferedImage image = QRCodeUtil.createQrCodeImage(data.getContent(), 256, 256);
        ImageIO.write(image, "png", response.getOutputStream());
    }

    @RequestMapping("scan")
    public ApiResult qrScanCount(@RequestParam("id") String id) {
        MockData data = dataService.get(id);
        IAssert.notNull(data, "未查找到对应数据");
        log.info("活动id = {},活动二维码扫描次数 = {}", id, data.getCount());
        dataService.updateScanCount(id);
        return ApiResult.success(data);
    }

    @Autowired
    private HttpServletRequest httpServletRequest;

    @GetMapping("/test")
    public ApiResult<Void> test() {
        System.out.println(httpServletRequest);
        System.out.println(httpServletRequest.getRequestURI());
        System.out.println(httpServletRequest.getServletPath());
        System.out.println(httpServletRequest.getSession().getId());
        return ApiResult.success();
    }

    public static void main(String[] args) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate.now().format(dateTimeFormatter);
        System.out.println(LocalDate.now().minusDays(20).format(dateTimeFormatter));
        System.out.println(LocalTime.now().compareTo(START_TIME));
        System.out.println(LocalTime.now().compareTo(END_TIME));
        System.out.println(LocalTime.now().isBefore(END_TIME_1));
    }

    private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm:ss");

    private static final LocalTime START_TIME = LocalTime.parse("17:15:00", TIME_FORMATTER);
    private static final LocalTime END_TIME = LocalTime.parse("20:30:00", TIME_FORMATTER);
    private static final LocalTime END_TIME_1 = LocalTime.parse("23:59:59", TIME_FORMATTER);
}
