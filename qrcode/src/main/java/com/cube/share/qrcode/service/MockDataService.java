package com.cube.share.qrcode.service;

import com.cube.share.qrcode.mock.MockData;
import com.cube.share.qrcode.utils.IdGenerator;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author cube.li
 * @date 2021/3/28 14:57
 * @description
 */
@Service
public class MockDataService {

    /**
     * 存放模拟数据,实际应当存放在数据库中
     */
    private static Map<String, MockData> mockDataMap = new ConcurrentHashMap<>();

    static {
        String id = IdGenerator.generatorStringId();
        mockDataMap.put(id, MockData.builder().id(id).content("欢迎参加挥泪大甩卖活动").count(0).build());
        String iD = IdGenerator.generatorStringId();
        mockDataMap.put(iD, MockData.builder().id(iD).content("这里是二维码的内容").count(0).build());
    }

    public void save(MockData data) {
        String id = IdGenerator.generatorStringId();
        data.setId(id);
        mockDataMap.put(id, data);
    }

    public MockData get(String id) {
        return mockDataMap.get(id);
    }

    public void updateScanCount(String id) {
        MockData existData = mockDataMap.get(id);
        mockDataMap.put(id, MockData.builder().id(id)
                .content(existData.getContent())
                .count(existData.getCount() + 1).build());
    }
}
