package com.cube.share.apiretry.annotations;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author cube.li
 * @date 2021/6/15 22:56
 * @description
 */
@SpringBootApplication
@ComponentScan("com.cube.share")
public class RetryApplication {

    public static void main(String[] args) {
        SpringApplication.run(RetryApplication.class, args);
    }
}
