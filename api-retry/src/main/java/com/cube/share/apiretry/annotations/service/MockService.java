package com.cube.share.apiretry.annotations.service;

import com.cube.share.apiretry.annotations.Retry;
import org.springframework.stereotype.Service;

/**
 * @author cube.li
 * @date 2021/6/15 22:51
 * @description
 */
@Service
public class MockService {

    private int count = 0;

    @Retry(value = "模拟业务", retryTimes = 2, executeInterval = 500)
    public void retry() {
        System.out.println("执行业务逻辑...");
        if (count++ == 0) {
            throw new RuntimeException();
        }
    }
}
