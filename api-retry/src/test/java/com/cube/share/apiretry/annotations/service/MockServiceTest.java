package com.cube.share.apiretry.annotations.service;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * @author cube.li
 * @date 2021/6/15 22:53
 * @description
 */
@SpringBootTest
class MockServiceTest {

    @Resource
    MockService mockService;

    @Test
    void retry() {
        mockService.retry();
    }
}