package com.cube.share.handler.service;

import com.cube.share.handler.config.ChineseNumber;
import com.cube.share.handler.config.PhaseStatus;
import com.cube.share.handler.config.SubmitStatus;
import com.cube.share.handler.entity.SysTask;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * @author poker.li
 * @date 2021/7/30 16:27
 */
@SpringBootTest
class SysTaskServiceTest {

    @Resource
    SysTaskService sysTaskService;

    @Test
    void save() {
        SysTask sysTask = new SysTask();
        sysTask.setName("任务3");
        sysTask.setDescription("任务三的描述");
        sysTask.setSubmitStatus(SubmitStatus.SUBMITTED);
        sysTask.setPhaseStatus(PhaseStatus.FINISHED);
        sysTask.setChineseNumber(ChineseNumber.THREE);
        sysTaskService.save(sysTask);
    }

    @Test
    void getOne() {
        SysTask sysTask = sysTaskService.getOne(11L);
        System.out.println(sysTask);
    }
}