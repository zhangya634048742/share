package com.cube.share.handler;

import cn.hutool.core.util.ClassUtil;
import com.cube.share.base.templates.IEnum;
import com.cube.share.handler.config.DataSourceConfiguration;
import com.cube.share.handler.config.PhaseStatusMappingHandler;
import com.cube.share.handler.config.SubmitStatus;
import com.cube.share.handler.entity.SysTask;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.SerializationUtils;
import org.junit.Test;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;

/**
 * @author poker.li
 * @date 2021/7/30 11:50
 */
public class JustTest {

    @Test
    public void enumTest() {
        System.out.println(IEnum.formCode(SubmitStatus.class, 1));
    }

    @Test
    public void justTest() {
        System.out.println(1 << 2);
    }

    @Test
    public void reflect() {
       /* Class clazz = AutoEnumTypeHandler.class;
        TypeVariable<Class<?>>[] typeVariables = clazz.getTypeParameters();
        System.out.println(clazz);
        System.out.println(typeVariables);*/

        Class clazz = PhaseStatusMappingHandler.class;
        TypeVariable<Class<?>>[] typeVariables = clazz.getSuperclass().getTypeParameters();
        Type type = clazz.getGenericSuperclass();
        Type[] types = clazz.getGenericInterfaces();
        System.out.println(clazz);
        System.out.println(typeVariables);
        System.out.println(type);
        System.out.println(types);
        System.out.println(ClassUtil.getLocation(DataSourceConfiguration.class));
    }

    @Test
    public void hash() {
        System.out.println(SubmitStatus.class.hashCode());
        System.out.println(System.identityHashCode(SubmitStatus.class));
        SysTask sysTask = new SysTask();
        sysTask.setName("测试");
        sysTask.setDescription("描述");
        System.out.println(sysTask.hashCode());
        System.out.println(System.identityHashCode(sysTask));
        System.out.println(SysTask.class.getCanonicalName());
    }

    @Test
    public void superClass() {
        System.out.println(SysTask.class.getSuperclass());
        System.out.println(Object.class.getSuperclass());
    }

    @Test
    public void deepCopy() {
        Person person = new Person(1L, "李四", 19);

        long start1 = System.currentTimeMillis();
        for (int i = 0; i < 100000; i++) {
            person.clone();
        }
        long end1 = System.currentTimeMillis();
        System.out.println("对象拷贝耗时:" + (end1 - start1));

        long start2 = System.currentTimeMillis();
        for (int i = 0; i < 100000; i++) {
            SerializationUtils.clone(person);
        }
        long end2 = System.currentTimeMillis();
        System.out.println("工具拷贝耗时:" + (end2 - start2));
    }


    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public static class Person implements Cloneable, Serializable {

        private static final long serialVersionUID = 5077540239468286627L;
        private Long id;

        private String name;

        private Integer age;

        //@SuppressWarnings("MethodDoesntCallSuperMethod")
        @Override
        public Person clone() {
            try {
                return (Person) super.clone();
            } catch (CloneNotSupportedException e) {
                throw new RuntimeException("CloneNotSupportedException");
            }
        }
    }

}