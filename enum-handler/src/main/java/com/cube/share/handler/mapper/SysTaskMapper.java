package com.cube.share.handler.mapper;

import com.cube.share.handler.entity.SysTask;

/**
 * @author poker.li
 * @date 2021/7/30 15:57
 */
public interface SysTaskMapper {

    /**
     * 保存
     *
     * @param sysTask 实体
     */
    void save(SysTask sysTask);

    /**
     * 根据id查找
     *
     * @param id id
     * @return 实体
     */
    SysTask getOne(Long id);
}
