package com.cube.share.handler.config;

import com.cube.share.base.templates.IEnum;

/**
 * @author poker.li
 * @date 2021/7/30 11:31
 * <p>
 * 提交状态枚举类
 */
public enum SubmitStatus implements IEnum<Integer, String> {

    /**
     * 未提交
     */
    UN_SUBMIT(1, "未提交"),
    /**
     * 已提交
     */
    SUBMITTED(2, "已提交");

    private final Integer code;

    private final String desc;

    SubmitStatus(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    @Override
    public Integer getCode() {
        return this.code;
    }

    @Override
    public String getValue() {
        return this.desc;
    }
}
