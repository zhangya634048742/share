package com.cube.share.handler.service;

import com.cube.share.handler.entity.SysTask;
import com.cube.share.handler.mapper.SysTaskMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author poker.li
 * @date 2021/7/30 16:25
 * <p>
 * 任务服务类
 */
@Service
public class SysTaskService {

    @Resource
    private SysTaskMapper sysTaskMapper;

    public void save(SysTask sysTask) {
        sysTaskMapper.save(sysTask);
    }

    public SysTask getOne(Long id) {
        return sysTaskMapper.getOne(id);
    }
}
