package com.cube.share.handler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author poker.li
 * @date 2021/7/30 11:30
 * <p>
 * 启动类
 */
@SpringBootApplication
public class EnumApplication {

    public static void main(String[] args) {
        SpringApplication.run(EnumApplication.class, args);
    }
}
