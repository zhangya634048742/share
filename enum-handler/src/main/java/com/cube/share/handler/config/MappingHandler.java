package com.cube.share.handler.config;

/**
 * 数据库字段内容映射到实体字段内存处理接口
 *
 * @param <D> 数据库内容
 * @param <O> 实体内容
 * @author litb
 */
public interface MappingHandler<D, O> {

    /**
     * 数据库转实体
     *
     * @param d
     * @return
     */
    public O convert(D d);

    /**
     * 实体转数据库
     *
     * @param o
     * @return
     */
    public D reconvert(O o);

}
