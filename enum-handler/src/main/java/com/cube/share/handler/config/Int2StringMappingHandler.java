package com.cube.share.handler.config;


import java.lang.reflect.TypeVariable;

/**
 * @author litb
 * @date 2022/1/8 16:07
 */
public class Int2StringMappingHandler implements MappingHandler<Integer, String> {

    @Override
    public String convert(Integer f) {
        return (f != null) ? f.toString() : null;
    }

    @Override
    public Integer reconvert(String t) {
        return (t != null) ? Integer.valueOf(t) : null;
    }


    public static void main(String[] args) {
        TypeVariable[] parameters = IEnumMappingHandler.class.getTypeParameters();

    }
}

