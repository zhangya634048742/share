package com.cube.share.handler.config;

import com.cube.share.base.templates.IEnum;

/**
 * @author cube.li
 * @date 2021/7/31 22:22
 * @description 大写数字枚举类
 */
public enum ChineseNumber implements IEnum<String, Integer> {

    ONE("壹", 1),
    TWO("贰", 2),
    THREE("弎", 3);

    private final String code;

    private final Integer value;

    ChineseNumber(String code, Integer value) {
        this.code = code;
        this.value = value;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public Integer getValue() {
        return this.value;
    }
}
