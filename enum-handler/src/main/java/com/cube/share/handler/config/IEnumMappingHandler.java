package com.cube.share.handler.config;

import com.cube.share.base.templates.IEnum;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * @author litb
 * @date 2022/1/8 15:41
 */
public class IEnumMappingHandler<C, E extends Enum<?> & IEnum> implements MappingHandler<C, E> {

    private final Class<E> type;

    public IEnumMappingHandler() {
        this.type = getGenericType();
    }

    private Class<E> getGenericType() {
        Type type = getClass().getGenericSuperclass();
        //noinspection unchecked
        return (Class<E>) ((ParameterizedType) type).getActualTypeArguments()[1];
    }

    @Override
    public E convert(C c) {
        return null;
    }

    @Override
    public C reconvert(E o) {
        return null;
    }

    public Class<E> getType() {
        return type;
    }
}
