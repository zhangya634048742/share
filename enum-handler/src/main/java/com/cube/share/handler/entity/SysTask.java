package com.cube.share.handler.entity;

import com.cube.share.handler.config.ChineseNumber;
import com.cube.share.handler.config.OrdinalType;
import com.cube.share.handler.config.PhaseStatus;
import com.cube.share.handler.config.SubmitStatus;
import lombok.Data;

/**
 * @author poker.li
 * @date 2021/7/30 15:56
 * <p>
 * 任务实体
 */
@Data
public class SysTask {

    private Long id;

    private String name;

    private String description;

    private SubmitStatus submitStatus;

    private PhaseStatus phaseStatus;

    private ChineseNumber chineseNumber;

    private OrdinalType ordinalType;
}
