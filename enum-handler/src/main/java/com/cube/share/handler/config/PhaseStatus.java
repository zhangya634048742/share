package com.cube.share.handler.config;

import com.cube.share.base.templates.IEnum;

/**
 * @author poker.li
 * @date 2021/7/31 9:42
 * <p>
 * 阶段状态枚举类
 */
public enum PhaseStatus implements IEnum<Integer, String> {

    /**
     * 未开始
     */
    NOT_BEGUN(1, "未开始"),
    /**
     * 进行中
     */
    ONGOING(2, "进行中"),
    /**
     * 已结束
     */
    FINISHED(3, "已结束");

    private final Integer code;

    private final String desc;

    PhaseStatus(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    @Override
    public Integer getCode() {
        return this.code;
    }

    @Override
    public String getValue() {
        return this.desc;
    }
}
