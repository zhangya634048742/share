package com.cube.share.handler.config;

/**
 * @author cube.li
 * @date 2021/7/31 22:59
 * @description 顺序枚举类
 */
public enum OrdinalType {

    THREE,
    ONE,
    TWO
}
