package com.cube.share.mp.converter;

import com.cube.share.mp.dto.UserDto;
import com.cube.share.mp.entity.UserEntity;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * @author cube.li
 * @date 2022/1/19 22:23
 */
@Mapper(componentModel = "spring")
public interface UserConverter {

    /**
     * UserDto to UserDto
     *
     * @param entity entity
     * @return dto对象
     */
    @Mapping(source = "name", target = "userName")
    UserDto userEntity2Dto(UserEntity entity);

    /**
     * UserDto to UserEntity
     *
     * @param dto dto
     * @return 实体
     */
    @Mapping(source = "userName", target = "name")
    UserEntity userDto2Entity(UserDto dto);

    /**
     * 测试转换器内的默认方法
     *
     * @param str 字符串
     * @return 结果
     */
    default String subStr(String str) {
        if (StringUtils.isBlank(str)) {
            return null;
        }
        return str.substring(0, 1);
    }

    /**
     * 测试转换器内的默认方法
     * Ambiguous mapping methods found for mapping property "String name" to String: String subStr(String str), String subStr1(String str). See https://mapstruct.org/faq/#ambiguous for more info.
     *
     * @param str 字符串
     * @return 结果
     */
    default String subStr1(String str) {
        if (StringUtils.isBlank(str)) {
            return null;
        }
        return str.substring(0, 1);
    }
}
