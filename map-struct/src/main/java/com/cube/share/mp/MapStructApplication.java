package com.cube.share.mp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author cube.li
 * @date 2022/1/19 22:20
 */
@SpringBootApplication
public class MapStructApplication {

    public static void main(String[] args) {
        SpringApplication.run(MapStructApplication.class, args);
    }
}
