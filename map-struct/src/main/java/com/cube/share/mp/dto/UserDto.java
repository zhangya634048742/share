package com.cube.share.mp.dto;

import lombok.Data;

/**
 * @author cube.li
 * @date 2022/1/19 22:21
 */
@Data
public class UserDto {

    private String userName;

    private Integer age;

    private String address;

}
