package com.cube.share.mp.entity;

import lombok.Data;

/**
 * @author cube.li
 * @date 2022/1/19 22:22
 */
@Data
public class UserEntity {

    private String name;

    private Integer age;

    private String address;
}
