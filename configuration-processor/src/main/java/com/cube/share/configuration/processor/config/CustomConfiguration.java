package com.cube.share.configuration.processor.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author litb
 * @date 2022/2/24 15:43
 */
@Configuration
@ConfigurationProperties(prefix = "custom.config")
@Setter
@Getter
public class CustomConfiguration {

    /**
     * 核心大小
     */
    private int coreSize;

    /**
     * 最大大小
     */
    private int maxCoreSize;

    /**
     * 是否等待超时
     */
    private boolean awaitTimeout = false;

    /**
     * 前缀
     */
    private String prefix;

    /**
     * host配置
     */
    private Host host;

    /**
     * 字母集合
     */
    private List<String> letters = new ArrayList<>(Arrays.asList("a", "b"));

    @Getter
    @Setter
    public static class Host {

        /**
         * ip地址
         */
        private String ip;

        /**
         * 端口
         */
        private int port;
    }
}
